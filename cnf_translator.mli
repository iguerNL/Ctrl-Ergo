open Ast_translator
open Stypes

type t
val print : Format.formatter -> t -> unit
val hashed_form : NhForm.t -> t
val early_pruning : t -> t
val cnf       : t -> atom list list * atom list list * atom list
  
