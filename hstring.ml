(******************************************************************************)
(** Ce fichier founit un module pour "hash-conser" les chaines de caracteres **)
(******************************************************************************)
open Hashcons

type t = { node : string ; tag : int }

module HC = 
  Hashcons.Make(
    struct
      type t' = t
      type t  = t'
      let hash s = Hashtbl.hash s.node
      let equal s1 s2 = String.compare s1.node s2.node = 0
      let tag tag t = {t with tag = tag}
    end)

let make s = HC.hashcons { node = s; tag= -1 (*Dump*) }
let view s = s.node

let hash s = s.tag

let empty  = make ""

let compare s1 s2 = s1.tag - s2.tag

let equal   s1 s2 = s1.tag - s2.tag = 0

let ptag fmt hs = if !Options.dtags then Format.fprintf fmt "[Hs-Tag:%d]" hs.tag
let print fmt hs  = Format.fprintf fmt "%s%a" hs.node ptag hs

let print_stats fmt =  
  Format.fprintf fmt "Hstring:@.";
  HC.print_stats fmt
  
module M = Stdlib.Map.Make
  (struct
     type t' = t
     type t  = t'
     let compare = compare
   end)

module H = 
  Hashtbl.Make(struct
     type t' = t
     type t  = t'
     let equal = equal
     let hash = hash
  end)

let fresh =
  let cpt = ref 0 in
  fun () ->
    incr cpt;
    make ("!k" ^ (string_of_int !cpt))
