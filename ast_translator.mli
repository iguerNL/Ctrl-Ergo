exception Unsolvable

type ('a, 'b) combine = 
  | Leaf of Hstring.t
  | Arith of 'a 
  | Match of 'b

type 'a arith = { m : 'a; c : Numbers.Z.t }

module rec Combine :
  sig
    type t = (Arith.t, Match.t) combine
    module MDec : Stdlib.Map.S with type key = t
    val compare : t -> t -> int
    val equal   : t -> t -> bool
    val hash    : t -> int
    val print   : Format.formatter -> t -> unit
    val alien_of_arith : Arith.t -> t
    val arith_of_alien : t -> Arith.t
    val fold_leaves   : (Hstring.t -> 'a -> 'a) -> t -> 'a -> 'a
    val leaves_of_atom : (Hstring.t -> 'a -> 'a) -> Stypes.atom -> 'a -> 'a
    val xleaves  : t -> t list
    val subst    : t -> t -> t -> t
    val solve : t -> Numbers.Z.t -> (t * t) list
  end
and Arith :
  sig
    type r = Combine.t
    type t = Numbers.Z.t Combine.MDec.t arith
    val compare : t -> t -> int
    val equal   : t -> t -> bool
    val hash    : t -> int
    val print   : Format.formatter -> t -> unit
    val mk_literal : Ast.op -> r -> r -> Stypes.atom
    val mult_const : Numbers.Z.t -> t -> t
    val div_const  : Numbers.Z.t -> t -> t
    val add : t -> t -> t
    val sub : t -> t -> t
    val pgcd_coefs_monomes : t -> Numbers.Z.t
    val normal_form_pos : t -> Numbers.Z.t Combine.MDec.t * Numbers.Z.t
    val subst    : r -> r -> t -> t
  end
and Match :
  sig
    type r = Combine.t
    type t
  end
and Stypes :
  sig
    type view =
      | TRUE
      | VAR of Hstring.t
      | EQ of Combine.t * Numbers.Z.t
      | GE of Combine.t * Numbers.Z.t
      | PROXY of Hstring.t * int

    type var = {
      mutable view : view;
      vid : int;
      pa : atom;
      na : atom;
      mutable weight : float;
      mutable sweight : int;
      mutable seen : bool;
      mutable level : int;
      mutable reason : reason;
    }
    and atom = {
      var : var;
      opp : atom;
      aid : int;
      mutable watched : clause Vec.t;
      mutable is_true : bool;
      is_neg : bool
    }
    and clause = {
      name : string;
      mutable atoms : Stypes.atom Vec.t;
      mutable activity : float;
      mutable removed : bool;
      learnt : bool;
    }
    and reason = clause option

    val dummy_var : var
    val cmp_var : var -> var -> int
    val eq_var : var -> var -> bool
    val h_var : var -> int
    val tag_var : var -> int
    val vars     : var Vec.t
    val pr_var : Format.formatter -> var -> unit

    val mk_atom : view -> bool -> atom
    val mk_proxy : int -> view
    val cmp_atom : atom -> atom -> int
    val eq_atom : atom -> atom -> bool
    val h_atom : atom -> int
    val tag_atom : atom -> int
    val vrai_atom : atom
    val faux_atom : atom
    val dummy_atom : atom
    val pr_atom : Format.formatter -> atom -> unit
    val pr_atom_view : Format.formatter -> view * bool -> unit


    val dummy_clause : clause
    val mk_clause : atom list -> int -> int -> clause
    val pr_clause : Format.formatter -> clause -> unit
    val pr_clause2 : Format.formatter -> clause -> unit
  end
and NhForm :
  sig
    type t
    type view = 
      | UNIT of Stypes.atom
      | AND  of t list
      | OR   of t list
      | IFF of t * t
          
    val equal   : t -> t -> bool
    val compare : t -> t -> int
    val print   : Format.formatter -> t -> unit
    val print_stats : Format.formatter -> unit
    val vrai    : unit -> t
    val faux    : unit -> t
    val view    : t -> view
    val mk_not  : t -> t
    val mk_lit  : Stypes.atom -> t
    val mk_and  : t list -> t
    val mk_or   : t list -> t
    val mk_iff  : t -> t -> t
    val form_of_expr : Ast.expr  -> t
    val form_of_assertions : Ast.expr Queue.t -> t
  end

and Explanation : sig
  type exp = Stypes.atom
  type t
  exception Inconsistent of t
  val empty : t
  val singleton : exp -> t
  val union : t -> t -> t
  val is_empty : t -> bool
  val inter : t -> t -> t
  val compare : t -> t -> int
  val iter : (exp -> unit)  -> t -> unit
  val fold : (exp -> 'a -> 'a) -> t -> 'a -> 'a
    (** the int explication must be empty *)

  val print : Format.formatter -> t -> unit
  val size : t -> int
  val from_atom : Stypes.atom -> t
  val from_empty_clause : Stypes.clause -> t

  type fresh_exp
  val fresh_exp : t -> fresh_exp * t
  val fresh_remove : fresh_exp -> t -> t

end

(*
module HForm :
  sig
    type t
    val print : Format.formatter -> t -> unit
    val hashed_form : NhForm.t -> t
    val early_pruning : t -> t
    val cnf       : 
      t -> Stypes.atom list list * Stypes.atom list list * Stypes.atom list
  end
*)
