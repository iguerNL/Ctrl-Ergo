open Format
open Options
open Numbers
open Ast_translator
open Stypes

module Ex = Explanation
module A = Arith
module C = Combine
module Tp = Theory_propagation
module MD = C.MDec

module Ieqs = Lia_eqs_int
module Reqs = Lia_eqs_rat
module Ineqs = Lia_ineqs

type t =  { 
  ieqs : Ieqs.t;
  (*reqs : Reqs.t;*)
  ineqs : Ineqs.t;
  diseqs     : (A.t * A.t * Ex.t) list;
}

let empty = { 
  ieqs = Ieqs.empty; 
  (*reqs = Reqs.empty;*)
  ineqs = Ineqs.empty;
  diseqs  =[] }


let merge lia1 lia2 = 
  { ieqs = Ieqs.merge lia1.ieqs lia2.ieqs;
    ineqs = Ineqs.merge lia1.ineqs lia2.ineqs;
    diseqs = List.rev_append lia1.diseqs lia2.diseqs }

let print env = 
  Ieqs.print env.ieqs;
  (*Reqs.print env.reqs;*)
  Ineqs.print env.ineqs

let print_assume_atom_view av neg = 
  if !dsat || !dla then fprintf fmt "I assume %a@." pr_atom_view (av,neg)

let ieqs_nform env r_ex = Ieqs.env_nform env.ieqs r_ex
  
(*let reqs_nform env r_ex = Reqs.env_nform env.reqs r_ex*)


let assume_eq env tp p ex =
  if MD.is_empty p.m then
    if Z.is_zero p.c then env, tp
    else raise (Ex.Inconsistent ex)
  else
    let m, gcd = A.normal_form_pos p in
    let c, rem = Z.div_rem p.c gcd in
    if not (Z.is_zero rem) then raise (Ex.Inconsistent ex)
    else
      let p = {m=m; c = Z.zero} in
      (*let reqs = 
        Reqs.solve_eq env.reqs (C.alien_of_arith p, (Z.minus c), ex) in*)
      let ieqs, tp = 
        Ieqs.solve_eq env.ieqs tp (C.alien_of_arith p, (Z.minus c), ex) in
      {env with ieqs = ieqs (*reqs = reqs;*)}, tp

let assume_diseq env tp p ex = 
  if MD.is_empty p.m then
    if Z.is_zero p.c then raise (Ex.Inconsistent ex)
    else env, tp
  else
    let rpx, ex = ieqs_nform env (C.alien_of_arith p, ex) in
    let p = C.arith_of_alien rpx in
    let m, gcd = A.normal_form_pos p in
    let c, rem = Z.div_rem p.c gcd in
    if not (Z.is_zero rem) then env, tp
    else 
      let rpx = Arith {m=m; c = Z.zero} in
      (*let rpx, ex = env_nform env (C.alien_of_arith p,ex) in*)
      let ineqs, eq_opt = Ineqs.solve_diseq env.ineqs (rpx, (Z.minus c), ex) in
      match eq_opt with
        | None -> {env with ineqs=ineqs}, tp
        | Some eq ->
          (*let reqs = Reqs.solve_eq env.reqs eq in*)
          let ieqs, tp = Ieqs.solve_eq env.ieqs tp eq in
          {env with ieqs=ieqs; (*reqs=reqs;*) ineqs=ineqs}, tp
            
let assume_ineq env tp p ex = 
  let rpx, ex = ieqs_nform env (C.alien_of_arith p, ex) in
  let p = C.arith_of_alien rpx in
  let ineqs, eq_opt = Ineqs.solve_ineq env.ineqs p ex in
  match eq_opt with
    | None -> {env with ineqs=ineqs}, tp
    | Some eq ->
      (*let reqs = Reqs.solve_eq env.reqs eq in*)
      let ieqs, tp = Ieqs.solve_eq env.ieqs tp eq in
      {env with ieqs=ieqs; (*reqs=reqs;*) ineqs=ineqs}, tp
        
        
let assume learning env tp av neg ex =
  print env;
  print_assume_atom_view av neg;
  match neg, av with
    | false, EQ(t,z) -> 
      let p = C.arith_of_alien t in
      assume_eq env tp {p with c = Z.sub p.c z} ex
        
    | true , EQ(t,z) -> 
      let p = C.arith_of_alien t in
      let p = {p with c = Z.sub p.c z} in
      let env, tp = assume_diseq env tp p ex in
      let ppun = {p with c = Z.add p.c  Z.one} in
      let unmp = A.sub {m=MD.empty; c=Z.one} p in
      {env with diseqs = (ppun, unmp, ex) :: env.diseqs}, tp
    | false, GE(t,z) -> 
      let tp = if learning then Tp.apply_ineq tp t z ex false else tp in
      assume_ineq env tp (A.sub ({m=MD.empty; c=z}) (C.arith_of_alien t)) ex
        
    | true , GE(t,z) -> 
      let tp = if learning then Tp.apply_ineq tp t z ex true else tp in
      let p = C.arith_of_alien t in
      let p = {p with c = Z.sub p.c (Z.sub z Z.one)} in
      assume_ineq env tp p ex
        
    | _, (TRUE | VAR _ | PROXY _) -> env,tp


let rec normalize_ineqs_by_by_pending_rs env tp = 
  let p_rs, ieqs = Ieqs.pending_rs env.ieqs in
  match p_rs with
    | [] -> env, tp
    | _ ->
      let ineqs, equas = Ineqs.apply_sbt env.ineqs p_rs in
      let env = {env with ieqs=ieqs; ineqs=ineqs} in
      match equas with
        | [] -> env, tp
        | _  -> 
          let env, tp = 
            List.fold_left 
              (fun (env,tp) (x,n,ex) ->
                let p = C.arith_of_alien x in
                let p = {p with c = Z.sub p.c n} in
                assume_eq env tp p ex
              )(env,tp) equas 
          in
          normalize_ineqs_by_by_pending_rs env tp

let trivial_model env tp = 
  let env, tp = normalize_ineqs_by_by_pending_rs env tp in
  Ineqs.trivial_model env.ineqs, env, tp
    


let debug_case_splits av neg = 
  if !dsplit then begin
    let f n =  string_of_float n in
    fprintf fmt "[case-split on] %a@." pr_atom_view (av,neg);
    fprintf fmt "[cc] nb_case_splits:   %s@." (f !Options.nb_case_splits);
    fprintf fmt "[cc] size_case_splits: %s@." (f !Options.size_case_splits);
    fprintf fmt "[cc] nb_backtracks:    %s@." (f !Options.nb_backtracks);
    fprintf fmt "[cc] nb_backjumps:     %s@." (f !Options.nb_backjumps);
  end

let rec assume_expensive env tp dlvl = 
  let b, env, tp = trivial_model env tp in
  if b 
(* XXX appel de dfs-fm plus souvent *)
(*|| dlvl <> 0*) then env, tp
  else
    let ineqs, equas = Ineqs.dfs_fm env.ineqs in
    match equas with
        [] -> {env with ineqs=ineqs}, tp
      | _ ->
        let env, tp = 
          List.fold_left 
            (fun (env, tp) (x,n,ex) ->
              let p = C.arith_of_alien x in
              let p = {p with c = Z.sub p.c n} in
              assume_eq env tp p ex
            ) (env,tp) equas 
        in
        assume_expensive {env with ineqs = ineqs} tp dlvl


let rec exp_process env = 
  let env, _ = assume_expensive env Tp.empty 0 in
  case_split env
  
and case_split env = 
  if !dsplit then fprintf fmt "CASE-SPLIT ...@.";
  match Ineqs.case_split env.ineqs with
    | None -> fprintf fmt "[case-split] : None@."; env
    | Some (av,ex,s) -> 
      nb_case_splits :=  !nb_case_splits +. 1.;
      size_case_splits := Numbers.Q.to_float s;
      debug_case_splits av false;
      let fresh,ex = Ex.fresh_exp ex in
      try
        if !dsplit then fprintf fmt "TRY ...@.";
        exp_process (fst (assume false env Tp.empty av false ex))
      with Ex.Inconsistent dep as e ->
        try
          let dep = Ex.fresh_remove fresh dep in
          if !dsplit then fprintf fmt "BACKTRACK ...@.";
          nb_backtracks := !nb_backtracks +. 1.;
          exp_process (fst (assume false env Tp.empty av true dep))
        with Not_found ->
          nb_backjumps := !nb_backjumps +. 1.;
          if !dsplit then fprintf fmt "BACKJUMP ...@.";
          raise e

let case_split_analysis env = 
  (* revoir la maniere de traiter les diseqs,
     dans la branche else, il faut peut etre traiter les diseqs *)
  let _ = fprintf fmt "expensive_processing with case-split@." in
  exp_process env

(* XXX 
   fprintf fmt " nb diseqs = %d@." (List.length env.diseqs);
   let rec aux env l =
   match l with
   | [] -> exp_process env
   | (p1, p2, ex) :: l ->
   try aux (fst (assume_ineq env Tp.empty p1 ex)) l
   with Ex.Inconsistent dep -> aux (fst (assume_ineq env Tp.empty p2 ex)) l
  in
   aux env env.diseqs
  *)
