(******************************************************************************)
(** Ce fichier fournit un ensemble de modules qui contiennent des structures
    de donn�es avanc�es relatives aux valeurs semantiques, aux literaux et aux
    formules utilis�es par le d�monstrateur. **)
(******************************************************************************)

open Format
open Options
open Numbers
open Stdlib
open Ast

module HS = Hstring

module L = List

exception Unsolvable
exception E of int

let (tbl : expr HS.H.t) = HS.H.create 1001

(******************************************************************************)
(** Les types des th�ories et de leur combinaison **)
(******************************************************************************)

type ('a , 'b) combine =
  | Leaf  of Hstring.t
  | Arith of 'a
  | Match of 'b
      
type 'a arith =  { m : 'a; c : Z.t }
    
type ('a , 'b) pmatch = ('a * 'b) list
    
module type T_OrderedHashedType = sig
  type r
  type t
  val compare  : t -> t -> int
  val equal    : t -> t -> bool
  val hash     : t -> int
  val print    : formatter -> t -> unit
  val mk_value : expr -> r
  val subst    : r -> r -> t -> t
end

module type C_OrderedHashedType = sig
  type t
  val compare  : t -> t -> int
  val equal    : t -> t -> bool
  val hash     : t -> int
  val print    : formatter -> t -> unit
  val mk_value : expr -> t
  val subst    : t -> t -> t -> t
end

module rec Combine : sig
  include C_OrderedHashedType with type t = (Arith.t, Match.t) combine 
  module MDec : Map.S with type key = t
  val mk_formula : op -> expr -> expr -> NhForm.t
  val alien_of_arith : Arith.t -> t
  val arith_of_alien : t -> Arith.t
  val alien_of_match : Match.t -> t
  val match_of_alien : t -> Match.t
  val fold_leaves   : (Hstring.t -> 'a -> 'a) -> t -> 'a -> 'a
  val leaves_of_atom : (Hstring.t -> 'a -> 'a) -> Stypes.atom -> 'a -> 'a
  val xleaves  : t -> t list
  val solve : t -> Numbers.Z.t -> (t * t) list
end =
struct
  type t = (Arith.t, Match.t) combine

  let solve a b = Arith.solve a b

  let print2 fmt r = match r with
    | Arith p  -> fprintf fmt "Arith[%a]" Arith.print p
    | Match p -> fprintf fmt "Match[%a]" Match.print p
    | Leaf  h  -> fprintf fmt "Leaf[%a]" HS.print h

  let print1 fmt r = match r with
    | Arith p  -> fprintf fmt "%a" Arith.print p
    | Match p -> fprintf fmt "%a" Match.print p
    | Leaf  h  -> fprintf fmt "%a" HS.print h

  let print = if !verbose then print2 else print1

  let compare a b = match a, b with
    | Leaf h1  , Leaf h2   -> HS.compare h1 h2
    | Arith p1 , Arith p2  -> Arith.compare p1 p2
    | Match p1, Match p2   -> Match.compare p1 p2
    | Leaf _   ,  _        -> 1
    | _        ,Leaf _     -> -1
    | Arith _  ,  _        -> 1
    | _        ,Arith _    -> -1

  module MDec =
    Map.Make
      (struct type t' = t type t  = t' let compare a b = compare b a end)

  let equal a b = match a, b with
    | Leaf h1  , Leaf h2   -> HS.equal h1 h2
    | Arith p1 , Arith p2  -> Arith.equal p1 p2
    | Match p1, Match p2   -> Match.equal p1 p2
    | _                    -> false

  let hash a = 
    match a with
      | Leaf h  -> HS.hash h
      | Arith p -> Arith.hash p
      | Match p -> Match.hash p
    
  let fold_leaves add r acc = match r with
    | Leaf h  -> add h acc
    | Arith p  -> 
        MDec.fold
          (fun x c acc ->
             match x with Leaf h -> add h acc | _ -> assert false) p.m acc
    | Match _ -> assert false
  
  let leaves_of_atom add a acc = match a.Stypes.var.Stypes.view with
      | Stypes.TRUE       -> acc
      | Stypes.VAR h      -> add h acc
      | Stypes.EQ(r,z)    -> fold_leaves add r acc
      | Stypes.GE(r,z)    -> fold_leaves add r acc
      | Stypes.PROXY(h,_) -> add  h acc


  let xleaves r = match r with
    | Leaf h  -> [r]
    | Arith p -> MDec.fold (fun x c acc -> x :: acc) p.m []
    | Match _ -> assert false

  let is_alien {m=m; c=c} =
    (Z.is_zero c) &&
      (try
        let premier = ref false in
        MDec.iter
          (fun x c ->
             if !premier || not (Z.is_one c) then raise Exit;
             premier := true;
          )m;
        !premier
      with Exit -> false )
      

  let alien_of_arith p = if is_alien p then fst (MDec.choose p.m) else Arith p

  let arith_of_alien r = 
    match r with
      | Arith p -> p
      | _  -> { m = MDec.singleton r Z.one; c = Z.zero }
          
  let alien_of_match l = match l with
    | [] -> assert false
    | [[], p] -> alien_of_arith p
    | _ -> Match l

  let match_of_alien r = 
    match r with
      | Match l -> l
      | _ -> [[], arith_of_alien r]

  let subst x t r = match r with
    | Match _ -> assert false
    | Leaf _  -> if equal x r then t else r
    | Arith p -> alien_of_arith (Arith.subst x t p)

  let rec mk_value e = 
    assert (e.ty = Int);
    match e.desc with
      | Var s -> 
        let hs = HS.make s in
        begin 
          try mk_value (HS.H.find tbl hs)
          with Not_found -> Leaf hs
        end
      | Const (_, _) | App ((Add | Mult | Sub), _) -> Arith.mk_value e
      | App(Ite, _)    -> Match.mk_value e
      | _ -> assert false
          
  module MA : Map.S with type key = Arith.t =
    Map.Make(struct type t= Arith.t let compare = Arith.compare end)

  let mk_formula op a b = 
    assert (a.ty = Int);
    let a = mk_value a in
    let b = mk_value b in
    match a, b with
      | Match l, Match l' -> 
          let lines =
            L.fold_left
              (fun acc (ca,a) ->
                 L.fold_left
                   (fun acc (cb,b) ->
                      let aa = alien_of_arith a in
                      let bb = alien_of_arith b in
                      let f = NhForm.mk_lit (Arith.mk_literal op aa bb) in
                      let line = NhForm.mk_and (List.rev_append (f::ca) cb) in
                      line :: acc
                   )acc l'
              )[] l
          in
          NhForm.mk_or lines    
            
      | Match l, _  -> 
          let l_choices = 
            List.fold_left
              (fun lc (fl2,p) ->
                 let f3 = NhForm.mk_lit (Arith.mk_literal op (Arith p) b) in
                 (NhForm.mk_and (f3::fl2)) :: lc
              )[] l
          in
          NhForm.mk_or l_choices
            
      | _, Match l' ->
          let l_choices  =
            List.fold_left
              (fun lc (fl2,p) ->
                 let f3 = NhForm.mk_lit (Arith.mk_literal op a (Arith p)) in
                 (NhForm.mk_and (f3::fl2)) :: lc
              )[] l'
          in
          NhForm.mk_or l_choices
            
      | _ -> NhForm.mk_lit (Arith.mk_literal op a b)

  
end

(******************************************************************************)
(** Ce module fournit le type des polynomes de l'arithmetique et quelques
    op�rations qui vont avec **)
(******************************************************************************)
and Arith : sig 
  include T_OrderedHashedType with type t = Z.t Combine.MDec.t arith 
                              and type r = Combine.t
  val mk_literal : op -> r -> r -> Stypes.atom
  val mult_const : Z.t -> t -> t
  val div_const  : Z.t -> t -> t
  val add : t -> t -> t
  val sub : t -> t -> t
  val pgcd_coefs_monomes : t -> Z.t
  val normal_form_pos : t -> Z.t Combine.MDec.t * Z.t
  val solve : r -> Numbers.Z.t -> (r * r) list

end = struct
  
  module M = Combine.MDec
  type r = Combine.t

  type t = Z.t M.t arith

  let compare p1 p2 = 
    let c = Z.compare p1.c p2.c in
    if  c = 0 then M.compare Z.compare p1.m p2.m else c

  let equal a b  = compare a b = 0

  let hash p = 
    let h =
      M.fold
        (fun x c h -> h * 97 + (Combine.hash x + Z.hash c)) p.m (Z.hash p.c) in
    abs h

  let print_plus_const fmt c =
    if Z.is_zero c then ()
    else if Z.is_one c then fprintf fmt "+"
    else if Z.is_one (Z.minus c) then  fprintf fmt "-"
    else fprintf fmt "%s*" (Z.to_string c)
    
  let print fmt {m=m;c=c} =
    match M.bindings m with
      | [] -> fprintf fmt "%s"  (Z.to_string c)
      | (x,cc)::l ->
          if Z.is_one cc && Z.is_zero c then 
            fprintf fmt "%a" Combine.print x
          else fprintf fmt "%s*%a" (Z.to_string cc) Combine.print x;
          List.iter (fun (x,cc) -> fprintf fmt "%a%a"
                       print_plus_const cc Combine.print x) l;
          if not (Z.is_zero c) then
            fprintf fmt "+%s"  (Z.to_string c)

  let smt_const fmt c = 
    if Z.compare_to_0 c < 0 then fprintf fmt "(- %s)" (Z.to_string (Z.minus c))
    else fprintf fmt "%s" (Z.to_string c)
      
  let print_smt fmt {m=m;c=c} =
    fprintf fmt "(+ ";
    fprintf fmt "%a"  smt_const c;
    M.iter
      (fun x c -> fprintf fmt " (* %a %a)" smt_const c Combine.print x) m;
    fprintf fmt ")"
    

  let print fmt p =
    if !smt_pp then print_smt fmt p
    else print fmt p

  let mult_const n p = 
    if Z.is_zero n then { m = M.empty ; c = Z.zero }
    else 
      if Z.is_one n then p
      else { m = M.map (Z.mult n) p.m ; c = Z.mult n p.c }

  let div_const z p = 
    assert (not (Z.is_zero z));
    if Z.is_one z then p
    else {m = M.map (fun c -> Z.div c z) p.m ; c = Z.div p.c z}
      
  let add p1 p2 = 
    let fadd _ a b =
      let c = Z.add a b in if Z.is_zero c then None else Some c in
    { m = M.union fadd p2.m p1.m ; c = Z.add p1.c p2.c }
      
  let mult ({m=m1;c=c1} as p1) ({m=m2;c=c2} as p2) =
    if M.is_empty m1 then mult_const c1 p2 
    else if M.is_empty m2 then mult_const c2 p1
    else assert false

  let sub p1 p2 = add p1 (mult_const Z.minus_one p2)

  let zero = { m = M.empty; c = Z.zero }
  let one  = { m = M.empty; c = Z.one }

  let expand c_l l m = 
    match !m with
      | [] -> m := L.rev_map (fun (cd, a) -> cd, mult_const c_l a) l
      | mr -> 
          m := L.fold_left
            (fun acc (ca,a) -> 
               L.fold_left 
                 (fun acc (cb,b) -> 
                    (List.rev_append ca cb, add (mult_const c_l a) b)::acc)
                 acc mr
            )[] l
          
  let cmp (fl,t) (ml,s) = Arith.compare t s
    
  let rec mk_val e = 
    let v = match e.desc with 
      | Const (s, Numeral) -> {m=M.empty; c=Z.of_string s}
      | App (Sub, [e])     -> mult_const Z.minus_one (mk_val e)
      | App (Sub, [a;b])   -> sub (mk_val a) (mk_val b)
      | App (Add, [a;b])   -> add (mk_val a) (mk_val b)
      | App (Add, l)       -> L.fold_left (fun p e -> add (mk_val e) p) zero l
      | App (Mult, l)      -> L.fold_left (fun p e -> mult (mk_val e) p) one l
      | Var _              -> Combine.arith_of_alien (Combine.mk_value e)
      | App(Ite, _)        -> Combine.arith_of_alien (Match.mk_value e)
      | _ -> fprintf fmt "%a@." Ast.print e; assert false
    in
    let mt = ref [] in
    let vm = 
      M.mapi_filter
        (fun x c ->
           match x with
             | Match pm -> expand c pm mt; None
             | _ -> Some c
        )v.m 
    in
    match !mt with
      | [] -> v
      | l  ->
        if Z.is_zero v.c && M.is_empty vm then
          Combine.arith_of_alien (Combine.alien_of_match l)
        else
          let v = {v with m=vm} in
          let l = List.rev_map (fun (cd,lm) -> cd, add v lm) l in
          Combine.arith_of_alien (Combine.alien_of_match l)

  let mk_value e = Combine.alien_of_arith (mk_val e)

  let choose_m m =
    let tn = ref None in
    let fchoose x a = tn := Some (a, x); raise Exit in
    (try M.iter fchoose m with Exit -> ());
    match !tn with Some e -> e | _ -> raise Not_found

  let pgcd_coefs_monomes {m=m} = 
    try
      Z.abs 
        (M.fold 
           (fun _ c z -> 
              if Z.is_one z then raise Exit; Z.pgcd c z) m Z.zero)
    with Exit -> Z.one

  let normal_form_pos ({m=m} as p) =
    if M.is_empty m then m, Z.one
    else
      let gcd  = pgcd_coefs_monomes p in
      let a, _ = choose_m m in
      let sg = Z.sign a in
      let gcd = 
        if sg > 0 then gcd else if sg < 0 then Z.minus gcd else assert false in
      M.map (fun c -> Z.div c gcd) m, gcd

  let mk_trivial_eq c gcd =
    if Z.is_zero c then Stypes.vrai_atom else Stypes.faux_atom

  let mk_eq mm c gcd =
    let cc, rr = Z.div_rem c gcd in
    if not (Z.is_zero rr) then Stypes.faux_atom
    else 
      let view = 
        Stypes.EQ  (Combine.alien_of_arith {m=mm; c=Z.zero}, Z.minus cc) in
      Stypes.mk_atom view false

  let mk_trivial_geq c gcd = 
    assert (Z.equal gcd Z.one);
    if Z.compare_to_0 c >= 0 then Stypes.vrai_atom 
    else Stypes.faux_atom

  let mk_geq mm c gcd = 
    (* changement de cote pour cc en rationnel *)
    let cc = Q.div (Q.of_z c) (Q.of_z (Z.minus gcd)) in 
    let pp = {m=mm; c=Z.zero} in
    if Z.compare_to_0 gcd > 0 then
      let ccc = Q.ceiling cc in
      let view = Stypes.GE (Combine.alien_of_arith pp, Q.numerator ccc) in
      Stypes.mk_atom view false
    else
      let ccc = Q.floor cc in
      let cccc = Q.add ccc Q.one in
      let view = Stypes.GE (Combine.alien_of_arith pp, Q.numerator cccc) in
      Stypes.mk_atom view true

  let mk_literal op a b = 
    let p = sub (Combine.arith_of_alien a) (Combine.arith_of_alien b) in
    let p = match op with
      | Eq -> p
      | Ge -> p
      | Gt -> {p with c = Z.sub p.c Z.one}
      | Le -> mult_const Z.minus_one p
      | Lt -> mult_const Z.minus_one {p with c = Z.add p.c Z.one}
      | _ -> assert false
    in 
    let m, gcd = normal_form_pos p in
    match M.is_empty m, op with
      | true , Eq -> mk_trivial_eq p.c gcd
      | true , _  -> mk_trivial_geq p.c gcd
      | false, Eq -> mk_eq m p.c gcd
      | false, _  -> mk_geq m p.c gcd

  let subst x t p =
    try
      let c = M.find x p.m in
      let p = { p with m = M.remove x p.m } in
      add (mult_const c (Combine.arith_of_alien t)) p
    with Not_found -> p
    

  (* symmetric modulo p 131 *)
  let mod_sym a b = 
    let _, m = Z.div_rem a b in 
    let m = 
      if Z.compare_to_0 m < 0 then
        if Z.compare m (Z.minus b) >= 0 then Z.add m b else assert false
      else 
        if Z.compare m b <= 0 then m else assert false
    in
    if Z.compare m (Z.div b (Z.of_int 2)) < 0 then m else Z.sub m b
        
  let map_monomes f mp (a0, x0) =
    M.fold
      (fun x a  mp -> 
         let a = f a in 
         if Z.is_zero a then mp
         else M.add x a mp)
      mp (M.singleton x0 a0)

  let apply_subst sb v = 
    List.fold_left (fun v (x, p) -> Combine.subst x p v) v sb
      
  let alien_of_arith_l l = List.map (fun (x,p) -> x, Combine.alien_of_arith p) l

  let extract_min p = 
    try
      let (x, a) =
        M.fold
          (fun y b ((x, a) as acc) ->
             if Z.compare (Z.abs a) (Z.abs b) <= 0 then acc else y, b
          )p.m (M.choose p.m)
      in
      x, a, {p with m = M.remove x p.m}
    with Not_found -> assert false


  (* Decision Procedures. Page 131 *)
  let rec omega p =
    if !dsolve then fprintf fmt "omega: %a@." print p;
    
    (* 1. choix d'une variable donc le |coef| est minimal *)
    let x, a, p = extract_min p in 
    
    let sbs = []  in

    if Z.is_one a then
      (* 3.1. si a = 1 alors on a une substitution entiere pour x *)
      let p = mult_const Z.minus_one p in 
      (x, Combine.alien_of_arith p) :: (alien_of_arith_l sbs)
    else if Z.is_minus_one a then
      (* 3Combine..2. si a = -1 alors on a une subst entiere pour x*)
      (x, Combine.alien_of_arith p) :: (alien_of_arith_l sbs)
    else 
      (* 4. sinon, (|a| <> 1) et a <> 0 *)
      (* 4.1. on rend le coef a positif s'il ne l'est pas deja *)
      let a, p = 
        if Z.compare_to_0 a  > 0  then a, p
	else Z.minus a, mult_const Z.minus_one p
      in
      (* 4.2. on reduit le systeme *)
      omega_sigma sbs a x p


  and omega_sigma sbs a x p0 = 
    (* 1. on definie m qui vaut a + 1 *)
    let m = Z.add a Z.one in
    
    (* 2. on introduit une variable fraiche *)
    let sigma = Leaf (Hstring.fresh ()) in
    
    (* 3. l'application de la formule (5.63) nous donne la valeur du pivot x*)
    let mm_sigma = (Z.minus m, sigma) in
    let m_mod = map_monomes (fun a -> mod_sym a m) p0.m mm_sigma in
    
    (* 3.1. Attention au signe de b : 
       on le passe a droite avant de faire mod_sym, d'ou Z.minus *)
    let c_mod = Z.minus (mod_sym (Z.minus p0.c) m) in
    let p_mod = { m = m_mod ;  c = c_mod } in
    
    let sbs = (x, Combine.alien_of_arith p_mod) :: sbs in
    
    (* 4. on substitue x par sa valeur dans l'equation de depart. 
       Voir la formule (5.64) *)
    let p' = add (mult_const a p_mod) p0 in
    
    (* 5. on resoud sur l'equation simplifiee *)
    let sbs2 = solve_aux p' in
    
    (* 6. on normalise sbs par sbs2 *)
    let sbs =  List.map (fun (x, v) -> x, apply_subst sbs2 v) sbs in
    
    (* 7. on supprime les liaisons inutiles de sbs2 et on merge avec sbs *)
    let sbs2 = List.filter (fun (y, _) -> y <> sigma) sbs2 in
    List.rev_append sbs sbs2
        
      
  and solve_aux p = 
    if !dsolve then fprintf fmt "solve_aux: %a@." print p;
    if M.is_empty p.m then
      if not (Z.is_zero p.c) then raise Unsolvable
      else []
    else
      let gcd = pgcd_coefs_monomes p in
      let _, rem = Z.div_rem p.c gcd in
      if not (Z.is_zero rem) then raise Unsolvable;
      omega (div_const gcd p)

  let solve r z = 
    if !dsolve then
      fprintf fmt "[arith] solve %a = %s@." Combine.print r (Z.to_string z);
    let p = Combine.arith_of_alien r in
    let p = { p with c = Z.sub p.c z } in
    let sbs = solve_aux p in
    if !dsolve then begin
      fprintf fmt "[arith] solving %a = %s yields:@." 
        Combine.print r (Z.to_string z);
      let c = ref 0 in
      List.iter 
        (fun (p,v) -> 
           incr c;
           fprintf fmt " %d) %a |-> %a@." !c 
             Combine.print p Combine.print v) sbs
    end;
    assert
      (let r = List.fold_left (fun r (p,v) -> Combine.subst p v r) r sbs in
       let p = Combine.arith_of_alien r in
       M.is_empty p.m && Z.equal p.c z);
    sbs

end

(******************************************************************************)
(** Module Pour les Match resultant de la transformation des Ite sur les 
    termes **)
(******************************************************************************)
and Match : sig 
  include T_OrderedHashedType with type t = (NhForm.t list , Arith.t) pmatch
                              and type r = Combine.t
end = struct

  type r = Combine.t
  type t = (NhForm.t list, Arith.t) pmatch

  let subst _ _ _ = assert false

  let mk_value e = 
    match e.desc with 
      | App(Ite, [c;t1;t2]) -> 
        let c = NhForm.form_of_expr c in
        if NhForm.equal c (NhForm.vrai()) then Arith.mk_value t1
        else if NhForm.equal c (NhForm.faux()) then Arith.mk_value t2
        else
          let t1 = Arith.mk_value t1 in 
          let t2 = Arith.mk_value t2 in
          let nc = NhForm.mk_not c in
          let l1 = match t1 with 
            | Match l -> 
              List.fold_left (fun acc (p,r) -> (c:: p, r) :: acc)[] l
            | _ -> [[c], Combine.arith_of_alien t1]
          in
          let l2 = match t2 with 
            | Match l -> 
              List.fold_left
                (fun acc (p,r) -> 
                  (nc :: p, r) :: acc
                )[] l
            | _ -> [[nc], Combine.arith_of_alien t2]
          in
          Match (List.rev_append l1 l2)
            
      | _ -> fprintf fmt "%a@." Ast.print e; assert false

  let cmp (f,t) (m,s) =
    let c = Arith.compare t s in
    if c <> 0 then c
    else
      try
        List.iter2 
          (fun f1 f2 ->
             let c = NhForm.compare f1 f2 in if c <> 0 then raise (E c))f m;
        0
      with
        | E c -> c
        | Invalid_argument "List.iter2" -> List.length f - List.length m

  let compare l1 l2 =
    try
      List.iter2 
        (fun a b -> let c = cmp a b in if c <> 0 then raise (E c)) l1 l2;
      0
    with 
      | E n -> n
      | Invalid_argument _ ->  List.length l1 - List.length l2

  let equal a b = compare a b = 0
  
  let hash l = 
    List.fold_left
      (fun z (fl,t) -> 
         let flh = List.fold_left (fun acc f -> Hashtbl.hash f + acc) 1 fl in
         z + flh + Arith.hash t) 0 l
      
  let print_l fmt l = 
    match l with
      | [] -> fprintf fmt "true"
      | e::l ->
          fprintf fmt "(%a" NhForm.print e;
          List.iter (fprintf fmt " and %a" NhForm.print)l;
          fprintf fmt "@."

  let print fmt l =
    fprintf fmt "[ TMatchWith@.";
    List.iter
      (fun (fl,t) ->
         fprintf fmt "  | %a -> %a@."  print_l fl Arith.print t) l;
    fprintf fmt "]@."

end

(******************************************************************************)
(** Ce module fournit les structures de:
    - variables booleennes,
    - atomes,
    - clauses, 
    sur des valeurs s�mantiques **)
(******************************************************************************)
and Stypes : sig

  type view = 
    | TRUE
    | VAR of HS.t
    | EQ of Combine.t * Z.t
    | GE of Combine.t * Z.t
    | PROXY of HS.t * int

  type var = { 
    mutable view : view;
    vid : int ;
    pa : atom;
    na : atom;
    mutable weight : float;
    mutable sweight: int;
    mutable seen : bool;
    mutable level : int;
    mutable reason: reason
  }
  and atom = {
    var : var;
    opp : atom;
    aid : int;
    mutable watched : clause Vec.t;
    mutable is_true : bool;
    is_neg : bool
  }
  and clause = { 
    name : string; 
    mutable atoms : Stypes.atom Vec.t ; 
    mutable activity : float;
    mutable removed : bool;
    learnt : bool 
  }
  and reason = clause option

  val dummy_var   : var
  val cmp_var : var -> var -> int
  val eq_var   : var -> var -> bool
  val h_var    : var -> int
  val tag_var  : var -> int
  val vars     : var Vec.t
  val pr_var   : formatter -> var -> unit
  val mk_proxy : int -> view

  val mk_atom : view -> bool (*is_negated ? *) -> atom
  val cmp_atom : atom -> atom -> int
  val eq_atom   : atom -> atom -> bool
  val h_atom  : atom -> int
  val tag_atom   : atom -> int
  val vrai_atom  : atom
  val faux_atom  : atom
  val dummy_atom : atom
  val pr_atom : formatter -> atom -> unit
  val pr_atom_view : formatter -> view * bool -> unit

  val dummy_clause : clause
  val mk_clause  : atom list -> int -> int -> clause
  val pr_clause : formatter -> clause -> unit
  val pr_clause2 : formatter -> clause -> unit

end = struct

  type view = 
    | TRUE
    | VAR of HS.t
    | EQ of Combine.t * Z.t
    | GE of Combine.t * Z.t
    | PROXY of HS.t * int

  type hash_consed_var = {node : view; tag : int}

  module HCV = 
    Hashcons.Make
      (struct
         type t  = hash_consed_var

         let equal a b = match a.node, b.node with
           | TRUE     , TRUE      -> true
           | VAR h    , VAR k     -> HS.equal h k
           | EQ(t1,z1), EQ(t2,z2) -> Z.equal z1 z2 && Combine.equal t1 t2
           | GE(t1,z1), GE(t2,z2) -> Z.equal z1 z2 && Combine.equal t1 t2
           | PROXY(_,x), PROXY(_,y) -> x - y = 0
           | _                    -> false
               
         let hash a =
           match a.node with
             | TRUE     -> 0
             | VAR h    -> abs (5 * HS.hash h) + 1
             | EQ(t, z) -> abs (5 * (Combine.hash t + Z.hash z) + 2)
             | GE(t, z) -> abs (5 * (Combine.hash t + Z.hash z) + 3)
             | PROXY(_,n) -> abs (5 * n + 4)

         let tag tag a = { a with tag = tag }
       end)

  type var = {
    mutable view : view;
    vid : int ;
    pa : atom;
    na : atom;
    mutable weight : float;
    mutable sweight: int;
    mutable seen : bool;
    mutable level : int;
    mutable reason: reason
  }
  and atom = {
    var : var;
    opp : atom;
    aid : int;
    mutable watched : clause Vec.t;
    mutable is_true : bool;
    is_neg : bool
  }
  and clause = { 
    name : string; 
    mutable atoms : Stypes.atom Vec.t ; 
    mutable activity : float;
    mutable removed : bool;
    learnt : bool 
  }
  and reason = clause option

  (*** print functions **)

  let smt_const fmt c = 
    if Z.compare_to_0 c < 0 then fprintf fmt "(- %s)" (Z.to_string (Z.minus c))
    else fprintf fmt "%s" (Z.to_string c)

  let pr_view fmt v = match v with
    | TRUE    -> fprintf fmt "true"
    | VAR h   -> fprintf fmt "%a" HS.print h
    | EQ(t,z) -> fprintf fmt "(= %a %a)" Combine.print t smt_const z
    | GE(t,z) -> fprintf fmt "(>= %a %a)" Combine.print t smt_const z
    | PROXY(h,_) -> fprintf fmt "%a" HS.print h

  let pr_var fmt v = pr_view fmt v.view

  let reason fmt a =
    match a.var.reason with
      | None -> fprintf fmt "@@%d" a.var.level
      | Some c -> fprintf fmt "->%s/%d" c.name a.var.level

  let val_of_atom fmt a =
    if a.is_true then fprintf fmt "[1%a]" reason a
    else if a.opp.is_true then fprintf fmt "[0%a]" reason a

  let pr_atom_view fmt (av,neg) =
    if neg then fprintf fmt "(not %a)" pr_view av
    else fprintf fmt "%a" pr_view av

  let pr_atom fmt a =
    if a.is_neg then fprintf fmt "(not %a)%a" pr_view a.var.view val_of_atom a
    else fprintf fmt "%a%a" pr_var a.var val_of_atom a
    
  let pr_vec_atoms fmt vc = 
    fprintf fmt "%a" pr_atom (Vec.get vc 0);
    for i=1 to Vec.size vc -1 do fprintf fmt ", %a" pr_atom (Vec.get vc i) done
      
  let pr_clause fmt {name=name; atoms=vec} =
    fprintf fmt "%s:{%a}" name pr_vec_atoms vec

  let pr_clause2 fmt {name=name; atoms=vec; learnt=learnt; removed=removed} =
    fprintf fmt "%s:%s{ %a}" 
      (if learnt then "L" else "C") 
      (if removed then "removed" else "")
      pr_vec_atoms vec


  let rec dummy_var =
    { vid     = -100;
      view    = TRUE;
      pa      = dummy_atom;
      na      = dummy_atom; 
      level   = -1;
      reason  = None;
      weight  = -1.;
      sweight = -1;
      seen = false
    }
  and dummy_atom = 
    { var     = dummy_var; 
      watched = {Vec.dummy=dummy_clause; data=[||]; sz=0};
      opp     = dummy_atom;
      is_true = false;
      aid     = -100;
      is_neg  = false
    }
  and dummy_clause = 
    { name     = ""; 
      atoms    = {Vec.dummy=dummy_atom; data=[||]; sz=0};
      activity = -1.;
      removed  = false;
      learnt   = false 
    }

  let rec vraie_var =
    { vid     = -1;
      view    = TRUE;
      pa      = vrai_atom;
      na      = faux_atom; 
      level   = -1;
      reason  = None;
      weight  = -1.;
      sweight = -1;
      seen = false
    }
  and vrai_atom = 
    { var     = vraie_var; 
      watched = {Vec.dummy=dummy_clause; data=[||]; sz=0};
      opp     = faux_atom;
      is_true = true;
      aid     = -2;
      is_neg  = false
    }
  and faux_atom = 
    { var     = vraie_var; 
      watched = {Vec.dummy=dummy_clause; data=[||]; sz=0};
      opp     = vrai_atom;
      is_true = false;
      aid     = -1;
      is_neg  = true
    }

  let vars = Vec.make 1000 dummy_var

  let mk_atom_aux view is_negated =
    let {tag=tag} = HCV.hashcons {node=view; tag = -1 (*Dumb*)} in
    let tag_fois_2 = tag lsl 1 in
    let var = if tag < Vec.size vars then Vec.get vars tag
    else 
      let rec var = 
        { view = view;
          vid = tag;
	  pa = pa;
	  na = na; 
	  level = -1;
	  reason = None;
	  weight = 0.;
	  sweight = 0;
	  seen = false}
      and pa =
        { var = var; 
	  watched = Vec.make 10 dummy_clause; 
	  opp = na;
	  is_true = false;
	  aid = tag_fois_2; (* aid = vid*2 *)
          is_neg  = false
        }
      and na = 
        { var = var;
	  watched = Vec.make 10 dummy_clause;
	  opp = pa;
	  is_true = false;
	  aid = tag_fois_2 + 1; (* aid = vid*2+1 *) 
          is_neg  = true
        } 
      in
      Vec.push vars var;
      var
    in
    assert (Vec.get vars var.vid == var);
    if is_negated then var.na else var.pa

  let mk_atom view is_negated =
    match view with
      | TRUE -> if is_negated then faux_atom else vrai_atom
      | _ -> mk_atom_aux view is_negated

  let cmp_var v1 v2 = v1.vid - v2.vid
  let eq_var v1 v2 = v1.vid - v2.vid = 0
  let tag_var v = v.vid
  let h_var v = v.vid

  let cmp_atom a1 a2 = a1.aid - a2.aid
  let eq_atom   a1 a2 = a1.aid - a2.aid = 0
  let h_atom a1 = a1.aid
  let tag_atom  a1 = a1.aid
    
  let fresh_lname =
    let cpt = ref 0 in 
    fun () -> incr cpt; "L" ^ (string_of_int !cpt)
      
  let fresh_dname =
    let cpt = ref 0 in 
    fun () -> incr cpt; "D" ^ (string_of_int !cpt)
      
  let fresh_name =
    let cpt = ref 0 in 
    fun () -> incr cpt; "C" ^ (string_of_int !cpt)

  let mk_clause ali sz_ali kind = 
    let name, is_learnt = match kind with
      | 0 -> fresh_name  (), false
      | 1 -> fresh_lname (), true
      | 2 -> fresh_dname (), false
      | _ -> assert false
    in
    let atoms = Vec.from_list ali sz_ali dummy_atom in
    { name  = name;
      atoms = atoms;
      removed = false;
      learnt = is_learnt;
      activity = 0. }

  let mk_proxy  n =
    let s = "PROXY__" ^ (string_of_int n) in
    PROXY (HS.make s, n)

end

and NhForm : sig
  type t
    
  type view = 
    | UNIT of Stypes.atom
    | AND  of t list
    | OR   of t list
    | IFF of t * t

  val equal   : t -> t -> bool
  val compare : t -> t -> int
  val print   : formatter -> t -> unit
  val print_stats : Format.formatter -> unit
  val vrai    : unit -> t
  val faux    : unit -> t
  val view    : t -> view
  val mk_not  : t -> t
  val mk_lit  : Stypes.atom -> t
  val mk_and  : t list -> t
  val mk_or   : t list -> t
  val mk_iff  : t -> t -> t
  val form_of_expr : expr  -> t
  val form_of_assertions : expr Queue.t -> t

end =
struct

  open Stypes

  type view = 
    | UNIT of atom
    | AND  of t list
    | OR   of t list
    | IFF  of t * t

  and t = { pos : view ; neg : view }

  let cpt = ref 0
  let sp() = let s = ref "" in for i = 1 to !cpt do s := " " ^ !s done; !s ^ !s

  let rec print_aux fmt fa = 
    match fa with
      | UNIT a -> fprintf fmt "%a"pr_atom a
      | IFF (f1,f2) -> fprintf fmt ";;<->\n(= %a %a)" print f1 print f2
      | AND s  -> 
          incr cpt;
          fprintf fmt "(and%a" print_list s;
          decr cpt;
          fprintf fmt "@.%s)" (sp())
            
      | OR s   -> 
          incr cpt;
          fprintf fmt "(or%a" print_list s;
          decr cpt;
          fprintf fmt "@.%s)" (sp())
                                                
  and print_list fmt s =
    match s with
      | [] -> fprintf fmt "(PK-vide ?)"
      | e::l ->
          List.iter(fprintf fmt "@.%s%a " (sp()) print) l;
          fprintf fmt "@.%s%a" (sp()) print e

  and print fmt f = print_aux fmt f.pos

  let print fmt f = cpt := 0; print fmt f

  let print_stats fmt = fprintf fmt "NhForm:@."
  
  let view f = f.pos

  let rec compare f1 f2 = match f1.pos, f2.pos with
    | AND l1, AND l2 -> compare_list l1 l2
    | AND _, _ -> 1
    | _, AND _ -> -1
    | OR l1, OR l2 -> compare_list l1 l2
    | OR _, _ -> 1
    | _, OR _ -> -1
    | IFF (f1, f2), IFF(s1,s2) ->
        let c = compare f1 s1 in
        if c <> 0 then c else compare  f2 s2
    | IFF (f1, f2), _ -> 1
    | _, IFF(s1,s2) -> -1
    | UNIT a1, UNIT a2 -> cmp_atom a1 a2
        
  and compare_list l1 l2 = 
    try
      List.iter2
        (fun f1 f2 ->
           let c = compare f1 f2 in if c <> 0 then raise (E c))l1 l2; 0
    with E c -> c

  let equal f1 f2 = compare f1 f2 = 0

  let make pos neg = {pos=pos; neg=neg}
    
  let complements f1 f2 = f1.pos = f2.neg

  let mk_lit a = make (UNIT a) (UNIT a.opp)

  let mk_not {pos=pos; neg=neg} = {pos=neg; neg=pos}
    
  let vrai = make (UNIT vrai_atom)(UNIT faux_atom)
  let faux = mk_not vrai

  let mk_or l =
    try 
      let l = 
        List.fold_left
          (fun acc f ->
            match f.pos with
              | OR l   -> List.rev_append acc l
              | UNIT a ->
                if a.is_true then raise Exit;
                if a.opp.is_true then acc
                else f :: acc
              | _      -> f :: acc
          )[] l
      in
      match l with
        | []  -> faux
        | [e] -> e
        | _   -> make (OR l) (AND (List.rev_map mk_not l))
    with Exit -> vrai
  
  let mk_and l =
    try 
      let l = 
        List.fold_left
          (fun acc f -> 
            match f.pos with
              | AND l -> List.rev_append acc l
              | UNIT a ->
                if a.opp.is_true then raise Exit;
                if a.is_true then acc
                else f :: acc
              | _     -> f :: acc
          )[] l
      in
      match l with
        | []  -> vrai
        | [e] -> e
        | _   -> make (AND l) (OR (List.rev_map mk_not l))
    with Exit -> faux
        
  let mk_iff f1 f2 = make (IFF(f1,f2)) (IFF(mk_not f1,f2))

  let add_eq vars e =
    match e.desc with
      | App(Eq, ([{desc=Var v} ; {desc=Const (_, Numeral)} as c] | 
          [{desc=Const (_, Numeral)} as c; {desc=Var v}])) ->
        let hs = HS.make v in
        if HS.H.mem tbl hs then vars
        else begin
          HS.H.add tbl hs c;
          (hs,e) :: vars
        end
      
      | App(Eq, [{desc=Var v1} as w1; {desc=Var v2} as w2]) ->
        let hs1 = Hstring.make v1 in
        let hs2 = Hstring.make v2 in
        let cmp = Hstring.compare hs1 hs2 in
        if cmp = 0 then vars
        else
          let hs, w = if cmp > 0 then hs1, w2 else hs2, w1 in
          if HS.H.mem tbl hs then vars
          else begin
            HS.H.add tbl hs w;
            (hs, e) :: vars
          end
      
      | _ -> vars


  let rec form_of_expr t =
    assert (t.ty = Bool);
    match t.desc with
      | True  -> vrai
      | False -> faux
      | Var v -> mk_lit (mk_atom (VAR (Hstring.make v)) false)
          
      | App(Ite, [a;b;c]) ->
          let a = form_of_expr a in
          let b = form_of_expr b in
          let c = form_of_expr c in
          mk_or [mk_and [a;b]; mk_and [mk_not a;c]]
            
      | App(And, l) -> 
        let vars = List.fold_left add_eq [] l in
        let l = List.rev_map form_of_expr l in
        let l = 
          List.fold_left
            (fun acc (hs,e) -> 
              HS.H.remove tbl hs;
              (form_of_expr e) :: acc
            )l vars
        in
        mk_and l
      | App(Or, l) -> mk_or (List.rev_map form_of_expr l)
      | App(Eq,[a;b]) when a.ty=Bool-> 
        mk_iff (form_of_expr a) (form_of_expr b)
      | App(Not, [a]) -> mk_not (form_of_expr a)
      | App((Eq|Gt|Ge|Lt|Le) as op, [a;b]) -> Combine.mk_formula op a b
      | Let (_, _) | Const (_, _) |
            App((Mult|Sub|Add|Not|Ite|Eq|Gt|Ge|Lt|Le), _) -> assert false

  let form_of_assertions q = 
    let vars = Queue.fold add_eq [] q in
    let l = Queue.fold (fun acc e -> (NhForm.form_of_expr e)::acc) [] q in
    let l = 
      List.fold_left
        (fun acc (hs,e) -> 
          HS.H.remove tbl hs;
          (form_of_expr e) :: acc
        )l vars
    in
    HS.H.clear tbl;
    mk_and l


  let vrai () = vrai
  let faux () = faux

end

and Explanation : sig
  type exp = Stypes.atom
  type t
  exception Inconsistent of t
  val empty : t
  val singleton : exp -> t
  val union : t -> t -> t
  val is_empty : t -> bool
  val inter : t -> t -> t
  val compare : t -> t -> int
  val iter : (exp -> unit)  -> t -> unit
  val fold : (exp -> 'a -> 'a) -> t -> 'a -> 'a
    (** the int explication must be empty *)

  val print : Format.formatter -> t -> unit
  val size : t -> int
  val from_atom : Stypes.atom -> t
  val from_empty_clause : Stypes.clause -> t

  type fresh_exp
  val fresh_exp : t -> fresh_exp * t
  val fresh_remove : fresh_exp -> t -> t
end = struct
  open Stypes

  type exp = Stypes.atom

  module S = 
    Set.Make
      (struct 
         type t = exp
         let compare a b = a.aid - b.aid
       end)

  module MI = 
    Map.Make
      (struct 
         type t = int
         let compare a b = a - b
       end)
  module SI = MI.Set
      
  type t = S.t * SI.t

  exception Inconsistent of t

  let singleton e = S.singleton e, SI.empty
   
  let empty = S.empty, SI.empty

  let union (s1,si1) (s2,si2) = S.union s2 s1, SI.union si1 si2
  let inter (s1,si1) (s2,si2) = S.inter s2 s1, SI.inter si1 si2

  let is_empty (s,si) = S.is_empty s && SI.is_empty si

  let compare (s1,si1) (s2,si2) =
    let c = S.compare s1 s2 in
    if c <> 0 then c else SI.compare si1 si2

  let iter f (s,si) = assert (SI.is_empty si); S.iter f s

  let fold f (s,si) acc = assert (SI.is_empty si); S.fold f s acc
    
  let print fmt (s,si) =
    if true || !Options.verbose then begin
      fprintf fmt "EXP:[";
      S.iter (fprintf fmt "(%a)" Stypes.pr_atom) s;
      SI.iter (fprintf fmt "(%i)") si;
      fprintf fmt "]"
    end

  let size (s,si) = S.cardinal s + SI.cardinal si

  let from_atom a =  
    let lvl = a.Stypes.var.Stypes.level in
    if lvl < 0 then assert false
    else if lvl = 0 then singleton a
    else singleton a

  let from_empty_clause c =  
    let s = ref S.empty in
    for i = 0 to Vec.size c.atoms - 1 do
      let a = Vec.get c.atoms i in
      let lvl = a.Stypes.var.Stypes.level in
      if lvl < 0 then assert false
      else if lvl > 0 then s := S.add a.opp !s
    done;
    !s,SI.empty

  type fresh_exp = int
  let fresh_exp =
    let c = ref (-1) in
    fun ((s: S.t),si) -> incr c; !c,(s,SI.add !c si)

  let fresh_remove fresh (s,si) =
    s, SI.remove_or_fail fresh si

end


