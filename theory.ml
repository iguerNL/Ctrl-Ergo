open Format
open Options
open Ast_translator
open Stypes
open Stdlib

module Ex = Explanation
module C = Combine
module MH = Hstring.M
module SH = MH.Set
module MI = Map.Make(struct type t = int let compare a b = a - b end)
module SI = MI.Set

type t = 
    {
      dummy  : bool;
      tp : Theory_propagation.t;
      cid_of_vars : int MH.t;
      vars_of_cid : SH.t MI.t;
      clusters :  (Lia.t * bool) MI.t;
    }



let print env = 
  (*  Tp.print env.tp; *)
  fprintf fmt "--------- clusters' vars ------------------------@.";
  MI.iter
    (fun cid sh ->
      fprintf fmt "%d -> " cid;
      SH.iter (fprintf fmt "%a ," Hstring.print) sh;
      fprintf fmt "@.";
    )env.vars_of_cid
  
let empty = {
  dummy = false;
  tp = Theory_propagation.empty;
  cid_of_vars = MH.empty;
  vars_of_cid = MI.empty;
  clusters  = MI.empty
}

let dummy_empty = {empty with dummy = true}

let env_nform env r_ex = 
  MI.fold (fun _ (lia,_) r_ex -> Lia.ieqs_nform lia r_ex) env.clusters r_ex

let init vars = {empty with tp = Theory_propagation.init vars}

let learn env = 
  let tp, l = Theory_propagation.learn env.tp in
  {env with tp=tp}, l


let expensive_processing env =
  let cs = MI.map (fun (lia,b) -> Lia.case_split_analysis lia,b) env.clusters in
  {env with clusters=cs}
    

let debug_assume len  =
  if !dsat || !dla then begin
    fprintf fmt "###### THEORY ######@.";
    fprintf fmt "Tassume %d atoms@." len
  end


let new_cluster =
  let cpt = ref 0 in
  fun sh env ->
    incr cpt;
    let lia = Lia.empty in
    { env with
      clusters = MI.add !cpt (lia, false) env.clusters;
      cid_of_vars = 
        SH.fold
          (fun hs cid_of_vars -> MH.add hs !cpt cid_of_vars) 
          sh env.cid_of_vars;
      vars_of_cid = MI.add !cpt sh env.vars_of_cid}, !cpt
    
let merge_clusters (env,cid) cid' =
  try
    (*fprintf fmt "> merge %d et %d@." cid cid';
    print env;*)
    let lia, _  = MI.find cid  env.clusters in
    let lia', _ = MI.find cid' env.clusters in
    (*fprintf fmt "LIA=%d=@." cid;
    Lia.print lia;
    fprintf fmt "LIA'=%d=@." cid';
    Lia.print lia';*)
    let new_lia = Lia.merge lia lia' in
    let clusters = MI.remove cid' env.clusters in
    let clusters = MI.add cid (new_lia, false) clusters in
    
    let sh  = MI.find cid  env.vars_of_cid in 
    let sh' = MI.find cid' env.vars_of_cid in 
    let new_sh  = SH.union sh sh' in
    let vars_of_cid = MI.remove cid' env.vars_of_cid in
    let vars_of_cid = MI.add cid new_sh vars_of_cid in
    
    let cid_of_vars = 
      SH.fold
        (fun hs cid_of_vars -> MH.add hs cid cid_of_vars)
        sh' env.cid_of_vars
    in
    {env with 
      clusters=clusters; 
      vars_of_cid=vars_of_cid; 
      cid_of_vars=cid_of_vars}, cid
    
  with Not_found -> assert false
      
let add_new_vars env cid sh =
  let known_sh  = MI.find cid  env.vars_of_cid in 
  let new_sh  = SH.union sh known_sh in
  let vars_of_cid = MI.add cid new_sh env.vars_of_cid in
  let cid_of_vars = 
    SH.fold
      (fun hs cid_of_vars -> MH.add hs cid cid_of_vars)
      sh env.cid_of_vars
  in
  {env with vars_of_cid=vars_of_cid; cid_of_vars=cid_of_vars}, cid


let update_clusters px env =
  let cids, sh = 
    C.fold_leaves 
      (fun hs (si,sh) ->
        (try SI.add (MH.find hs env.cid_of_vars) si with Not_found -> si),
        SH.add hs sh
      )px (SI.empty, SH.empty)
  in
  match SI.elements cids with
    | [] -> new_cluster sh env
    | [cid] -> add_new_vars env cid sh
    | cid::l -> 
      let env, cid = 
        List.fold_left (fun acc cid' -> merge_clusters acc cid') (env, cid) l in
      add_new_vars env cid sh


let assume env facts lvl dlvl = 
  Timer.CC_assume.start ();
  let env = 
    if lvl < 0 || Queue.is_empty facts then env
    else 
      let _ = debug_assume (Queue.length facts) in
      let env = 
        Queue.fold 
          (fun env a ->
            assert (a.is_true);
            match a.var.view with
                EQ(px,_) | GE (px,_) ->
                  (*fprintf fmt "@.avant merge: poly=%a@." C.print px;
                  print env;*)
                  let env, cid = update_clusters px env in
                  (*fprintf fmt "apres merge@.";*)
                  (*print env;
                  fprintf fmt "################################@.";*)
                  let lia, tp = 
                    try 
                      let lia, _ = MI.find cid env.clusters in
                      let ex = Ex.from_atom a in
                      Lia.assume true lia env.tp a.var.view a.is_neg ex
                    with Not_found -> assert false
                  in
                  {env with
                    clusters = MI.add cid (lia, true) env.clusters; tp = tp}
              | _-> env
          ) env facts 
      in
      if lvl = 0 then env
      else
        let env = 
          MI.fold
            (fun cid (lia, b) env ->
              if b then
                let lia, tp  = Lia.assume_expensive lia env.tp dlvl in
                { env with
                  clusters = MI.add cid (lia, false) env.clusters; tp = tp}
              else env
            ) env.clusters env
        in
        if lvl = 1 then env
        else assert false
  in
  Timer.CC_assume.stop ();
  env
    


let light_assume env a =
  match a.var.view with
      EQ(px,_) | GE (px,_) ->
        let env, cid = update_clusters px env in
        let lia, tp = 
          try 
            let lia, _ = MI.find cid env.clusters in
            Lia.assume true lia env.tp a.var.view a.is_neg Ex.empty
          with Not_found -> assert false
        in
        {env with 
          clusters = MI.add cid (lia, true) env.clusters; tp=tp}
    | _-> env
  
