(******************************************************************************)
(** Ce fichier founit un module pour "hash-conser" les chaines de caracteres **)
(******************************************************************************)

open Hashcons 

type t

val make : string -> t

val view : t -> string

val equal : t -> t -> bool

val compare : t -> t -> int

val hash : t -> int

val empty : t 

val print : Format.formatter -> t -> unit

val print_stats : Format.formatter -> unit

val fresh : unit -> t

module M : Stdlib.Map.S with type key = t
module H : Hashtbl.S with type key = t
