open Ast_translator
open Stypes
open Format
open Options

exception Sat
exception Unsat
exception Restart
exception Conflict of clause

module T = Theory
module Ex = Explanation

let to_float i = float_of_int i
let to_int f = int_of_float f

module D = struct
  let d_decisions = !dsat
  let d = !dsat
  let d_trail = false
  let d_model = false

  let pr_list_atoms fmt l = List.iter (fprintf fmt "%a ; " pr_atom) l

  let enqueue a = 
    if d_decisions then fprintf fmt "I enqueue the atom %a@." pr_atom a
      
  let trail_content trail =
    if d_trail then begin
      fprintf fmt "The trail contrains:@.";
      for i = Vec.size trail - 1 downto 0 do
        fprintf fmt "  | %a@." pr_atom (Vec.get trail i);
      done;
      fprintf fmt "  --------------@."
    end

  let propagate a = 
    if d_decisions then fprintf fmt "I propagate the atom %a@." pr_atom a
      
  let theory_propagate a = 
    if d_decisions then fprintf fmt "I theory-propagate the atom %a@." pr_atom a
      
  let propagate_unit_clause cl = 
    if d_decisions then fprintf fmt "The clause %a becomes unit@." pr_clause cl
      
  let propagate_false_clause cl = 
    if d_decisions then fprintf fmt "The clause %a becomes FALSE@." pr_clause cl
      
  let propagate_true_clause cl = 
    if d_decisions then fprintf fmt "The clause %a becomes TRUE@." pr_clause cl

  let decide decisions lvl v =
    if d_decisions then 
      fprintf fmt "(%dth) I decide the atom %a at level %d@." 
        decisions pr_atom v lvl
        
  let boolean_conflict dep c_level conflict_cpt =
    if d_decisions then begin
      fprintf fmt "@.I have a (%d-th) boolean conflict at level %d@." 
        !conflict_cpt c_level;
      fprintf fmt "%d Empty clause:@.%a@." (Ex.size dep) Ex.print dep ;
    end
      
  let theory_conflict dep c_level conflict_cpt = 
    if d_decisions then begin
      fprintf fmt "@.I have a (%d-th) theory conflict at level %d@." 
        !conflict_cpt c_level;
      fprintf fmt "%d Empty clause:@.%a@." (Ex.size dep) Ex.print dep;
    end
(*
    let l = Ex.fold (fun e acc -> e::acc)dep [] in
    let l = List.fast_sort (fun a b -> b.var.level - a.var.level) l in
    match l with
      | [] |[_] -> ()
      | a::b::_ -> 
        if a.var.level <> b.var.level then
          if true || d_decisions then begin
            fprintf fmt "@.I have a (%d-th) theory conflict at level %d@." 
              !conflict_cpt c_level;
            fprintf fmt "%d Empty clause:@.%a@." (Ex.size dep) Ex.print dep ;
    end
*)
      
  let conflict source dep c_level conflict_cpt =
    if source = 1 then boolean_conflict dep c_level conflict_cpt
    else if source = 2 then theory_conflict dep c_level conflict_cpt
    else assert false

  let analyze_conflict blevel learnt = 
    if d_decisions then 
      fprintf fmt "Backjump to level %d and learn the clause { %a}@." 
        blevel pr_list_atoms learnt        


  let theory_learnt_clause blevel learnt = 
    if d_decisions then 
      fprintf fmt "Theory propagation at level %d and learn the clause { %a}@."
        blevel pr_list_atoms learnt        

  let watched_of_a a =
    let ws = a.watched in
    if false && d_decisions then begin
      fprintf fmt "%a watches the following clauses:@." pr_atom a;
      for i = 0 to Vec.size ws -1 do
        fprintf fmt " >%a@." pr_clause2 (Vec.get ws i)
      done
    end

  let add_clause cl = 
    if d_decisions then begin
      fprintf fmt "I add a clause: %a@." pr_clause2 cl;
      fprintf fmt "with the watch literals %a  and  %a@.@."
        pr_atom (Vec.get cl.atoms 0).opp
        pr_atom (Vec.get cl.atoms 1).opp
    end
      
  let add_clause_exists cl = 
    if d_decisions then
      fprintf fmt "the clause %a already exists in dn@." pr_clause2 cl
        
  let add_unit_clause a = 
    if d then fprintf fmt "I DON'T add the unit clause: %a@." pr_atom a

  let print_model vec = 
    if d_model then begin
      fprintf fmt "MODEL: @.";
      for i = 0 to Vec.size vec - 1 do 
        let v = Vec.get vec i in
        fprintf fmt "%a@." pr_atom v.pa;
        assert (v.pa.is_true || v.na.is_true)
      done
    end        

  let check_clause c = 
    if d then eprintf "@.check_clause: %a@." Stypes.pr_clause c;
    let atoms = c.atoms in
    let b = ref false in
    for i=0 to Vec.size atoms - 1 do b := !b || (Vec.get atoms i).is_true done;
    assert (!b)
      
  let check_vec vec = 
    for i = 0 to Vec.size vec - 1 do check_clause (Vec.get vec i) done
      
  let check_model vars clauses learnts = 
    print_model vars;
    check_vec clauses;
    check_vec learnts

end

module Dnet = struct

  module M = Map.Make (struct type t = int let compare a b = a - b end)
    
  type dnet = Leaf of clause | Node of dnet M.t
      
  exception Exists of clause
      
  let to_dnet l clause = 
    let rec to_dnet l = match l with
      | []   -> Node (M.add 0 (Leaf clause) M.empty)
      | a::l -> Node (M.add a.aid (to_dnet l) M.empty)
    in
    to_dnet l
      
  let rec remove_bigger mp = 
    M.iter
      (fun i v ->
        match v with
          | Leaf c ->
            c.removed <- true;
           (*fprintf fmt "@.%a@." atoms_array c.atoms;*)
          | Node mp -> remove_bigger mp;
      )mp


  let update_dnet l dn clause = dn(*
    let rec add l dn = 
      match dn, l with
        | Leaf _, _ -> assert false
        | Node mp, [] -> 
          (try match M.find 0 mp with
            | Leaf c ->
              if c.removed then Node (M.add 0 (Leaf clause) mp)
              else 
                begin
                  fprintf fmt "exists@.";
                  raise (Exists c)
                end
            | _      -> assert false
           with Not_found -> 
             fprintf fmt "smaller@.";
             (*fprintf fmt "@.===========================@.smaller: %a @.@."
               atoms_array clause.atoms;
             *)
             remove_bigger mp;
             Node (M.add 0 (Leaf clause) mp))
        | Node mp, a::l -> 
          if M.is_empty mp then fprintf fmt "bigger@.";
          try
            let sub_dn = M.find a.aid mp in
            let sub_dn = add l sub_dn in
            Node (M.add a.aid sub_dn mp)
          with Not_found -> 
            Node (M.add a.aid (to_dnet l clause) mp)
    in 
    add l dn*)
    

end

(*==============================================================================
  Traduction de Solver.h
  *===========================================================================*)
type env = 
    { 

      (* si vrai, les contraintes sont deja fausses *)
      mutable unsat : bool;
      
      (* clauses du probleme *)
      mutable clauses : clause Vec.t;
      
      (* clauses apprises *)
      mutable learnts : clause Vec.t;
      
      (* valeur de l'increment pour l'activite des clauses *)
      mutable clause_inc : float;
      
      (* valeur de l'increment pour l'activite des variables *)
      mutable var_inc : float;
      
      (* un vecteur des variables du probleme *)
      mutable vars : var Vec.t;
      
      (* la pile de decisions avec les faits impliques *)
      trail : atom Vec.t;

      (* une pile qui pointe vers les niveaux de decision dans trail *)
      trail_lim : int Vec.t;

      (* Tete de la File des faits unitaires a propager. 
	 C'est un index vers le trail *)
      mutable qhead : int;
      
      (* Nombre des assignements top-level depuis la derniere 
         execution de 'simplify()' *)
      mutable simpDB_assigns : int;

      (* Nombre restant de propagations a faire avant la prochaine 
         execution de 'simplify()' *)
      mutable simpDB_props : int;

      (* Un tas ordone en fonction de l'activite des variables *)
      mutable order : Heap.t;

      (* estimation de progressions, mis a jour par 'search()' *)
      mutable progress_estimate : float;

      (* *)
      remove_satisfied : bool;

      (* inverse du facteur d'acitivte des variables, vaut 1/0.999 par defaut *)
      var_decay : float;

      (* inverse du facteur d'activite des clauses, vaut 1/0.95 par defaut *)
      clause_decay : float;

      (* la limite de restart initiale, vaut 100 par defaut *)
      mutable restart_first : int;

      (* facteur de multiplication de restart limite, vaut 1.5 par defaut*)
      restart_inc : float;
      
      (* limite initiale du nombre de clause apprises, vaut 1/3 
         des clauses originales par defaut *)
      mutable learntsize_factor : float;
      
      (* multiplier learntsize_factor par cette valeur a chaque restart, 
         vaut 1.1 par defaut *)
      learntsize_inc : float;

      (* controler la minimisation des clauses conflit, vaut true par defaut *)
      expensive_ccmin : bool;

      (* controle la polarite a choisir lors de la decision *)
      polarity_mode : bool;
      
      mutable starts : int;

      mutable decisions : int;

      mutable propagations : int;

      mutable conflicts : int;

      mutable clauses_literals : int;

      mutable learnts_literals : int;

      mutable max_literals : int;

      mutable tot_literals : int;

      mutable nb_init_vars : int;

      mutable nb_init_clauses : int;
      
      model : var Vec.t;
      
      mutable tenv : T.t;

      tenv_queue : T.t Vec.t;
      
      tatoms_queue : atom Queue.t;

      mutable dnet : Dnet.dnet;

    }
      
      

let env =
  { 
    unsat = false;   
    
    clauses = Vec.make 0 dummy_clause; (*sera mis a jour lors du parsing*)
    
    learnts = Vec.make 0 dummy_clause; (*sera mis a jour lors du parsing*)
    
    clause_inc = 1.;
    
    var_inc = 1.;
    
    vars = Vec.make 0 dummy_var; (*sera mis a jour lors du parsing*)
    
    trail = Vec.make 601 dummy_atom;

    trail_lim = Vec.make 601 (-105);

    qhead = 0;
    
    simpDB_assigns = -1;

    simpDB_props = 0;

    order = Heap.init 0; (* sera mis a jour dans solve *)

    progress_estimate = 0.;

    remove_satisfied = true;

    var_decay = 1. /. 0.95;

    clause_decay = 1. /. 0.999;

    restart_first = 100;(*100;*)

    restart_inc = 1.5;
    
    learntsize_factor = 1. /. 3. ;
    
    learntsize_inc = 1.1;

    expensive_ccmin = true;

    polarity_mode = false;
    
    starts = 0;

    decisions = 0;

    propagations = 0;

    conflicts = 0;

    clauses_literals = 0;

    learnts_literals = 0;

    max_literals = 0;

    tot_literals = 0;

    nb_init_vars = 0;

    nb_init_clauses = 0;
    
    model = Vec.make 0 dummy_var;
    
    tenv = T.empty;

    tenv_queue = Vec.make 100 T.dummy_empty;

    tatoms_queue = Queue.create ();

    dnet = Dnet.Node Dnet.M.empty

  }


let f_weight i j = 
  (Vec.get env.vars j).weight < (Vec.get env.vars i).weight
    
let f_filter i = (Vec.get env.vars i).level < 0

let insert_var_order v = 
  if not (Heap.in_heap env.order v.vid) then 
    Heap.insert f_weight env.order v.vid

let var_decay_activity () = env.var_inc <- env.var_inc *. env.var_decay
  
let clause_decay_activity () = 
  env.clause_inc <- env.clause_inc *. env.clause_decay

let var_bump_activity v = 
  v.weight <- v.weight +. env.var_inc;
  if v.weight > 1e100 then begin
    for i = 0 to env.vars.Vec.sz - 1 do
      (Vec.get env.vars i).weight <- (Vec.get env.vars i).weight *. 1e-100
    done;
    env.var_inc <- env.var_inc *. 1e-100;
  end;
  if Heap.in_heap env.order v.vid then begin
    if false && D.d_decisions then eprintf "decrease %d with new weight %f@." 
      (v.vid+1) v.weight;
    Heap.decrease f_weight env.order v.vid
  end

let clause_bump_activity c = 
  c.activity <- c.activity +. env.clause_inc;
  if c.activity > 1e20 then begin
    for i = 0 to env.learnts.Vec.sz - 1 do
      (Vec.get env.learnts i).activity <-
        (Vec.get env.learnts i).activity *. 1e-20;
    done;
    env.clause_inc <- env.clause_inc *. 1e-20
  end

let decision_level () = Vec.size env.trail_lim

let nb_assigns () = Vec.size env.trail
let nb_clauses () = Vec.size env.clauses
let nb_learnts () = Vec.size env.learnts
let nb_vars    () = Vec.size env.vars

let new_decision_level() = 
  Vec.push env.trail_lim (Vec.size env.trail);
  Vec.push env.tenv_queue env.tenv (* save the current tenv *)

(*==============================================================================
  Traduction de Solver.C
  *===========================================================================*)

let attach_clause c = 
  Vec.push (Vec.get c.atoms 0).opp.watched c;
  Vec.push (Vec.get c.atoms 1).opp.watched c;
  if c.learnt then 
    env.learnts_literals <- env.learnts_literals + Vec.size c.atoms
  else
    env.clauses_literals <- env.clauses_literals + Vec.size c.atoms
      
let detach_clause c = 
  c.removed <- true;
  (*
    Vec.remove (Vec.get c.atoms 0).neg.watched c;
    Vec.remove (Vec.get c.atoms 1).neg.watched c;
  *)
  if c.learnt then 
    env.learnts_literals <- env.learnts_literals - Vec.size c.atoms
  else
    env.clauses_literals <- env.clauses_literals - Vec.size c.atoms

let remove_clause c = detach_clause c

let satisfied c = 
  try
    for i = 0 to Vec.size c.atoms - 1 do
      if (Vec.get c.atoms i).is_true then raise Exit
    done;
    false
  with Exit -> true

let unassign_atom a = 
  a.is_true     <- false;
  a.opp.is_true <- false;
  a.var.level   <- -1;
  a.var.reason  <- None


(* annule tout jusqu'a lvl exclu *)
let cancel_until lvl = 
  if decision_level () > lvl then begin
    env.qhead <- Vec.get env.trail_lim lvl;
    for c = Vec.size env.trail - 1 downto env.qhead do
      let a = Vec.get env.trail c in
      unassign_atom a;
      insert_var_order a.var
    done;
    Queue.clear env.tatoms_queue;
    env.tenv <- Vec.get env.tenv_queue lvl; (* recover the right tenv *)
    Vec.shrink env.trail ((Vec.size env.trail) - env.qhead);
    Vec.shrink env.trail_lim ((Vec.size env.trail_lim) - lvl);
    Vec.shrink env.tenv_queue ((Vec.size env.tenv_queue) - lvl)
  end;
  assert (Vec.size env.trail_lim = Vec.size env.tenv_queue)

let rec pick_branch_lit () =
  if Heap.size env.order = 0 then begin
    if false then fprintf fmt "Empty-heap -> sat ?? @.";
    raise Sat
  end
  else
    let max = Heap.remove_min f_weight env.order in
    let v = Vec.get env.vars max in
    if v.level>= 0 then  begin
      assert (v.pa.is_true || v.na.is_true);
      pick_branch_lit ()
    end
    else v

let enqueue a lvl reason = 
  D.enqueue a;
  assert (not a.is_true && not a.opp.is_true && 
            a.var.level < 0 && a.var.reason = None && lvl >= 0);
  let reason = if lvl = 0 then None else reason in
  a.is_true <- true;
  a.var.level <- lvl;
  a.var.reason <- reason;
  Vec.push env.trail a;
  D.trail_content env.trail

let progress_estimate () = 
  let prg = ref 0. in
  let nbv = to_float (nb_vars()) in
  let lvl = decision_level () in 
  let _F = 1. /. nbv in
  for i = 0 to lvl do
    let _beg = if i = 0 then 0 else Vec.get env.trail_lim (i-1) in
    let _end = if i=lvl then Vec.size env.trail else Vec.get env.trail_lim i in
    prg := !prg +. _F**(to_float i) *. (to_float (_end - _beg))
  done;
  !prg /. nbv


let bottom c = 
  try
    for i = 0 to Vec.size c.atoms - 1 do
      let a = Vec.get c.atoms i in
      if a.is_true || not (a.is_true || a.opp.is_true) then raise Exit
    done;
    true
  with Exit -> false

let look_for_empty_clauses c =
  (*fprintf fmt "################################@.";
    fprintf fmt "j'ai trouve %a@.mais aussi:@." pr_clause c;*)
  let cpt = ref 0 in
  for i = 0 to Vec.size env.clauses - 1 do
    let c = Vec.get env.clauses i in
    if bottom c then begin
      incr cpt;
    (*fprintf fmt "  %a@." pr_clause c;*)
    end
  done;
  for i = 0 to Vec.size env.learnts - 1 do
    let c = Vec.get env.learnts i in
    if bottom c then begin
      incr cpt;
    (*fprintf fmt "  %a@." pr_clause c;*)
    end
  done;
  fprintf fmt "#Empty_clauses:%d@." !cpt


let propagate_in_clause a c i watched new_sz =
  let atoms = c.atoms in
  let first = Vec.get atoms 0 in
  if first == a.opp then begin (* le litiral faux doit etre dans .(1) *)
    Vec.set atoms 0 (Vec.get atoms 1);
    Vec.set atoms 1 first
  end;
  let first = Vec.get atoms 0 in
  if first.is_true then begin
    (* clause vraie, la garder dans les watched *)
    Vec.set watched !new_sz c; 
    incr new_sz;
    D.propagate_true_clause c;
  end
  else 
    try (* chercher un nouveau watcher *)
      for k = 2 to Vec.size atoms - 1 do
        let ak = Vec.get atoms k in
        if not (ak.opp.is_true) then begin 
          (* Watcher Trouve: mettre a jour et sortir *)
          Vec.set atoms 1 ak;
          Vec.set atoms k a.opp;
          Vec.push ak.opp.watched c;
          raise Exit
        end
      done;
      (* Watcher NON Trouve *)
        if first.opp.is_true then begin
        (* la clause est fausse *)
        (*look_for_empty_clauses c;*)
          D.propagate_false_clause c;
          env.qhead <- Vec.size env.trail;
          for k = i to Vec.size watched - 1 do
            Vec.set watched !new_sz (Vec.get watched k);
            incr new_sz;
          done;
          raise (Conflict c)
        end
        else begin
        (* la clause est unitaire *)
          D.propagate_unit_clause c;
          Vec.set watched !new_sz c; 
          incr new_sz;
          enqueue first (decision_level ()) (Some c)
        end
    with Exit -> ()
      
let propagate_atom a res = 
  D.propagate a;
  let watched = a.watched in
  D.watched_of_a a;
  let new_sz_w = ref 0 in
  begin 
    try
      for i = 0 to Vec.size watched - 1 do
        let c = Vec.get watched i in
        if not c.removed then propagate_in_clause a c i watched new_sz_w
      done;
    with Conflict c -> assert (!res = None); res := Some c 
  end;
  let dead_part = Vec.size watched - !new_sz_w in
  Vec.shrink watched dead_part;
  D.watched_of_a a

let expensive_theory_propagate () =
  try
    if D.d then eprintf "expensive_theory_propagate@.";
    ignore(T.expensive_processing env.tenv);
    if D.d then eprintf "expensive_theory_propagate => None@.";
    None
  with Ex.Inconsistent dep -> 
    if D.d then eprintf "expensive_theory_propagate => Inconsistent@.";
    Some dep
     
(* 
let ckeck_unsat fait afaire = 
  if !verbose then fprintf fmt "ici: decision_level = %d@." (decision_level());
  let th_env = 
    if decision_level()=0 then env.tenv
    else Vec.get env.tenv_queue 0 in
  let qfait = Queue.create () in
  List.iter (fun e -> Queue.add e qfait) fait;
  let qafaire = Queue.create () in
  List.iter (fun e -> Queue.add e qafaire) afaire;
  let th_env = T.assume th_env qfait 0 (decision_level()) in
  let th_env = T.assume th_env qafaire 1 (decision_level())in 
  ()
(* in
  ignore(T.expensive_processing th_env)*)

let rec minimize_dep fait afaire =
  if D.d_decisions then 
    fprintf fmt "size: %d+%d@." (List.length fait) (List.length afaire);
  match afaire with
    | [] -> 
      (try ckeck_unsat fait []; assert false
       with Ex.Inconsistent dep -> dep)

    | a::afaire' ->
      try
        ckeck_unsat fait afaire';
        minimize_dep (a::fait) afaire'
      with Ex.Inconsistent dep ->
        minimize_dep fait afaire'
*)  

let theory_propagate () = 
  try 
    env.tenv <- T.assume env.tenv env.tatoms_queue 1 (decision_level());
    Queue.clear env.tatoms_queue;
    if nb_assigns() = env.nb_init_vars then expensive_theory_propagate ()
    else None
  with Ex.Inconsistent dep -> 
    Queue.clear env.tatoms_queue;
    if D.d_decisions then 
      fprintf fmt "=======================================================@.";
    if !verbose then fprintf fmt "UNSAT-CORE A REDUIRE:@.";
    
    (***
    let l =
      Ex.fold 
        (fun a acc->
          D.theory_propagate a;
          if a.is_true then a :: acc
          else 
            if a.opp.is_true then a.opp :: acc
            else assert false
        ) dep [] 
      in
      Timer.Solver_minimize_dep.start();
      let res = minimize_dep [] l in
      Timer.Solver_minimize_dep.stop();
      if D.d_decisions then 
      fprintf fmt "=======================================================@.";
      Some res
    **)
    Some dep


let propagate () = 
  let num_props = ref 0 in
  let res = ref None in
  (*assert (Queue.is_empty env.tqueue);*)
  while env.qhead < Vec.size env.trail do
    let a = Vec.get env.trail env.qhead in
    env.qhead <- env.qhead + 1;
    incr num_props;
    propagate_atom a res;
    Queue.push a env.tatoms_queue;
  done;
  env.propagations <- env.propagations + !num_props;
  env.simpDB_props <- env.simpDB_props - !num_props;
  !res


let analyze c_clause = 
  if D.d then eprintf "conflicting clause: %a@." pr_clause c_clause;
  let pathC  = ref 0 in
  let learnt = ref [] in
  let cond   = ref true in
  let blevel = ref 0 in
  let seen   = ref [] in
  let c      = ref c_clause in
  let tr_ind = ref (Vec.size env.trail - 1) in
  let size   = ref 1 in
  while !cond do
    if !c.learnt then clause_bump_activity !c;

    (* visit the current predecessors *)
    for j = 0 to Vec.size !c.atoms - 1 do
      let q = Vec.get !c.atoms j in
      (*printf "I visit %a@." D.atom q;*)
      assert (q.is_true || q.opp.is_true && q.var.level >= 0); (* Pas sur *)
      if not q.var.seen && q.var.level > 0 then begin
        var_bump_activity q.var;
        q.var.seen <- true;
        seen := q :: !seen;
        if q.var.level >= decision_level () then incr pathC
        else begin
          learnt := q :: !learnt;
          incr size;
          blevel := max !blevel q.var.level
        end
      end
    done;
    
    (* look for the next node to expand *)
    while not (Vec.get env.trail !tr_ind).var.seen do decr tr_ind done;
    decr pathC;
    let p = Vec.get env.trail !tr_ind in
    decr tr_ind;
    match !pathC, p.var.reason with
      | 0, _ -> 
        cond := false;
        learnt := p.opp :: (List.rev !learnt)
      | n, None   -> assert false
      | n, Some cl -> c := cl
  done;
  List.iter (fun q -> q.var.seen <- false) !seen;
  !blevel, !learnt, !size

let f_sort_db c1 c2 = 
  let sz1 = Vec.size c1.atoms in
  let sz2 = Vec.size c2.atoms in
  let c = Pervasives.compare c1.activity c2.activity in
  if sz1 = sz2 && c = 0 then 0
  else 
    if sz1 > 2 && (sz2 = 2 || c < 0) then -1
    else 1

let locked c = false
(*
  try
  for i = 0 to Vec.size env.vars - 1 do
  match (Vec.get env.vars i).reason with
  | Some c' when c ==c' -> raise Exit
  | _ -> ()
  done;
  false
  with Exit -> true*)
  
let reduce_db n_of_learnts = 
  let n_of_learnts = 2 * n_of_learnts / 3 in
  let size = Vec.size env.learnts in
  Vec.sort env.learnts (fun c1 c2 -> Pervasives.compare c2.activity c1.activity);
  if false then fprintf fmt "reduce db@.";
  (*for i = 0 to size - 1 do
    fprintf fmt ">%g@." (Vec.get env.learnts i).activity
  done;*)
  for i = n_of_learnts to size - 1 do
    remove_clause (Vec.get env.learnts i);
  done;
  Vec.shrink env.learnts (size - n_of_learnts);

  (*fprintf fmt "size=%d@." size;
  fprintf fmt "n_of_learnt=%d@." n_of_learnts;
  fprintf fmt "new size=%d@." (Vec.size env.learnts);*)
  
    
  
  ()
(*
  let n_of_l = to_int ((to_float (nb_clauses())) *. env.learntsize_factor) in
  let len = Vec.size env.learnts in
  let n_of_l_10 = 10 * n_of_l in
  if len > n_of_l_10 then begin
    Vec.sort env.learnts f_sort_db;
    for i = n_of_l_10 to len - 1 do (Vec.get env.learnts i).removed <- true done;
                       (*eprintf "len=%d@." len;
                         eprintf "10*n_of_l=%d@." n_of_l_10;*)
    Vec.shrink env.learnts (len - n_of_l_10);
                       (*eprintf "len'=%d@." (Vec.size env.learnts)*)
  end;
  
  let extra_lim = env.clause_inc /. (to_float (Vec.size env.learnts)) in
  Vec.sort env.learnts f_sort_db;
  let lim2 = Vec.size env.learnts in
  let lim1 = lim2 / 2 in
  let j = ref 0 in
  for i = 0 to lim1 - 1 do
    let c = Vec.get env.learnts i in
    if Vec.size c.atoms > 2 && not (locked c) then 
      remove_clause c
    else 
      begin Vec.set env.learnts !j c; incr j end
  done;
  for i = lim1 to lim2 - 1 do 
    let c = Vec.get env.learnts i in
    if Vec.size c.atoms > 2 && not (locked c) && c.activity < extra_lim then
      remove_clause c
    else 
      begin Vec.set env.learnts !j c; incr j end
  done;
  Vec.shrink env.learnts (lim2 - !j)
*)

let theory_satisfied c =
  try
    let nb_bot = ref 0 in
    for i = 0 to Vec.size c.atoms - 1 do
      let a = Vec.get c.atoms i in
      match a.is_neg, a.var.view with
        | false, EQ(t,z) -> 
          let t, _ = T.env_nform env.tenv (t,Ex.empty) in
          let tz = Arith {m=Combine.MDec.empty; c=z} in
          let b = Arith.mk_literal Ast.Eq t tz in
          if false && not (Stypes.eq_atom a b) then begin
            fprintf fmt "t_normalize %a@." pr_atom a;
            fprintf fmt "res =       %a@." pr_atom b
          end; 
          if Stypes.eq_atom b Stypes.vrai_atom then begin
            fprintf fmt "%a est vrai dans env@." pr_atom a;
            raise Exit;
          end;
          if Stypes.eq_atom b Stypes.faux_atom then begin
            fprintf fmt "%a est faux dans env@." pr_atom a;
            incr nb_bot
          end
            
        | true , EQ(t,z) -> 
          let t, _ = T.env_nform env.tenv (t,Ex.empty) in
          let tz = Arith {m=Combine.MDec.empty; c=z} in
          let b = Arith.mk_literal Ast.Eq t tz in
          let b = Stypes.mk_atom b.Stypes.var.Stypes.view true in
          if false && not (Stypes.eq_atom a b) then begin
            fprintf fmt "t_normalize %a@." pr_atom a;
            fprintf fmt "res =       %a@." pr_atom b
          end;
          if Stypes.eq_atom b Stypes.faux_atom then begin
            fprintf fmt "%a est vrai dans env@." pr_atom a;
            raise Exit;
          end;
          if Stypes.eq_atom b Stypes.vrai_atom then begin
            fprintf fmt "%a est faux dans env@." pr_atom a;
            incr nb_bot
          end   
      | false, GE(t,z) -> 
        let t, _ = T.env_nform env.tenv (t,Ex.empty) in
        let tz = Arith {m=Combine.MDec.empty; c=z} in
        let b = Arith.mk_literal Ast.Ge t tz in
        if false && not (Stypes.eq_atom a b) then begin
          fprintf fmt "t_normalize %a@." pr_atom a;
          fprintf fmt "res =       %a@." pr_atom b
        end;
        if Stypes.eq_atom b Stypes.vrai_atom then begin
          fprintf fmt "%a est vrai dans env@." pr_atom a;
          raise Exit;
        end;
        if Stypes.eq_atom b Stypes.faux_atom then begin
          fprintf fmt "%a est faux dans env@." pr_atom a;
          incr nb_bot
        end
            
      | true , GE(t,z) -> 
        let t, _ = T.env_nform env.tenv (t,Ex.empty) in
        let tz = Arith {m=Combine.MDec.empty; c=z} in
        let b = Arith.mk_literal Ast.Ge t tz in
        let b = Stypes.mk_atom b.Stypes.var.Stypes.view true in
        if false && not (Stypes.eq_atom a b) then begin
          fprintf fmt "t_normalize %a@." pr_atom a;
          fprintf fmt "res =       %a@." pr_atom b
        end;
        if Stypes.eq_atom b Stypes.faux_atom then begin
          fprintf fmt "%a est vrai dans env@." pr_atom a;
          raise Exit;
        end;
        if Stypes.eq_atom b Stypes.vrai_atom then begin
          incr nb_bot;
          fprintf fmt "%a est faux dans env@." pr_atom a;
        end
            
      | _, (TRUE | VAR _ | PROXY _) -> ()
    done;
    assert (!nb_bot < Vec.size c.atoms);
    false
  with Exit -> 
    (*fprintf fmt "ICI@.";*)
    false


let rec theory_simplify () = 
  if !Options.theory_simplification >= 1 then begin
    for i = 0 to Vec.size env.vars - 1 do
      let a = (Vec.get env.vars i).pa in
      let b = 
        if a.is_true || a.opp.is_true then a
        else match a.is_neg, a.var.view with
          | false, EQ(t,z) -> 
            let t, _ = T.env_nform env.tenv (t,Ex.empty) in
            let tz = Arith {m=Combine.MDec.empty; c=z} in
            let b = Arith.mk_literal Ast.Eq t tz in
            if eq_atom b vrai_atom then (enqueue a 0 None; a)
            else if eq_atom b faux_atom then (enqueue a.opp 0 None; a)
            else b
              
          | true , EQ(t,z) -> 
            let t, _ = T.env_nform env.tenv (t,Ex.empty) in
            let tz = Arith {m=Combine.MDec.empty; c=z} in
            let b = (Arith.mk_literal Ast.Eq t tz).opp in
            if eq_atom b vrai_atom then (enqueue a.opp 0 None; a)
            else if eq_atom b faux_atom then (enqueue a 0 None; a)
            else b

          | false, GE(t,z) -> 
            let t, _ = T.env_nform env.tenv (t,Ex.empty) in
            let tz = Arith {m=Combine.MDec.empty; c=z} in
            let b = Arith.mk_literal Ast.Ge t tz in
            if eq_atom b vrai_atom then (enqueue a 0 None; a)
            else if eq_atom b faux_atom then (enqueue a.opp 0 None; a)
            else b
              
          | true , GE(t,z) -> 
            let t, _ = T.env_nform env.tenv (t,Ex.empty) in
            let tz = Arith {m=Combine.MDec.empty; c=z} in
            let b = (Arith.mk_literal Ast.Ge t tz).opp in
            if eq_atom b vrai_atom then (enqueue a.opp 0 None; a)
            else if eq_atom b faux_atom then (enqueue a 0 None; a)
            else b
              
          | _, (TRUE | VAR _ | PROXY _) -> a
      in
      if not (eq_atom a b) then
        if b.is_true then enqueue a 0 None
        else if b.opp.is_true then enqueue a.opp 0 None
        else begin
          if a.is_neg = b.is_neg then a.var.view <- b.var.view;
          if !Options.theory_simplification >= 2 then
            begin 
              try 
                ignore (T.light_assume env.tenv a);
                try ignore (T.light_assume env.tenv a.opp);
                with Ex.Inconsistent dep -> enqueue a 0 None
              with Ex.Inconsistent dep -> enqueue a.opp 0 None
            end
        end
    done;
    let head = env.qhead in
    if env.unsat || propagate () <> None || theory_propagate () <> None then 
      begin
        env.unsat <- true;
        raise Unsat
      end;
    let head' = env.qhead in
    if head' > head then theory_simplify ()
  end  


let remove_satisfied vec = 
  let j = ref 0 in
  let k = Vec.size vec - 1 in
  for i = 0 to k do
    let c = Vec.get vec i in
    if satisfied c then remove_clause c
    else
      begin
        Vec.set vec !j (Vec.get vec i);
        incr j
      end
  done;
  Vec.shrink vec (k + 1 - !j)
    
let simplify () =
  assert (decision_level () = 0);
  if env.unsat || propagate () <> None || theory_propagate () <> None then begin
    env.unsat <- true;
    raise Unsat
  end;
  if nb_assigns() <> env.simpDB_assigns && env.simpDB_props <= 0 then begin
    if D.d then fprintf fmt "simplify@.";
    theory_simplify ();
    if Vec.size env.learnts > 0 then remove_satisfied env.learnts;
    if env.remove_satisfied then remove_satisfied env.clauses;
    env.simpDB_assigns <- nb_assigns ();
    env.simpDB_props <- env.clauses_literals + env.learnts_literals;
  end

let record_learnt_clause blevel learnt size = 
  D.analyze_conflict blevel learnt;
  begin match learnt with
    | [] -> assert false
    | [fuip] ->
      assert (blevel = 0);
      enqueue fuip 0 None
    | fuip :: ll ->
      (* XXX EXPERIMENTAL *)
      let l = List.fast_sort (fun a b -> b.var.level - a.var.level) ll in
      let learnt = fuip :: l in
      (*/ XXX *)
      
      let lclause = mk_clause learnt size 1 in
      try
        let l = List.fast_sort (fun a b -> a.var.vid - b.var.vid) learnt in
        env.dnet <- Dnet.update_dnet l env.dnet lclause;
        Vec.push env.learnts lclause;
        attach_clause lclause;
        D.watched_of_a (Vec.get lclause.atoms 0).opp;
        D.watched_of_a (Vec.get lclause.atoms 1).opp;

        clause_bump_activity lclause;
        enqueue fuip blevel (Some lclause)
          
      with Dnet.Exists _ -> 
        D.add_clause_exists lclause;
        clause_bump_activity lclause;
        enqueue fuip blevel (Some lclause)

  end;
  var_decay_activity ();
  clause_decay_activity()

let record_theory_learnt_clause learnt size = 
  let blevel = decision_level () in
  D.theory_learnt_clause blevel learnt;
  begin match learnt with
    | [] -> assert false
    | [fuip] ->
      assert (blevel = 0);
      enqueue fuip 0 None
    | fuip :: ll ->
      (* XXX EXPERIMENTAL *)
      let l = List.fast_sort (fun a b -> b.var.level - a.var.level) ll in
      let learnt = fuip :: l in
      (*/ XXX *)

      let lclause = mk_clause learnt size 1 in
      try
        let l = List.fast_sort (fun a b -> a.var.vid - b.var.vid) learnt in
        env.dnet <- Dnet.update_dnet l env.dnet lclause;
        Vec.push env.learnts lclause;
        attach_clause lclause;
        D.watched_of_a (Vec.get lclause.atoms 0).opp;
        D.watched_of_a (Vec.get lclause.atoms 1).opp;

        clause_bump_activity lclause;
        enqueue fuip blevel (Some lclause)
          
      with Dnet.Exists _ -> 
        D.add_clause_exists lclause;
        clause_bump_activity lclause;
        enqueue fuip blevel (Some lclause)

  end;
  var_decay_activity ();
  clause_decay_activity()

let check_inconsistence_of dep = ()(*
                                     try
                                     let env = ref (T.empty()) in
                                     Ex.iter
                                     (fun atom -> env := T.assume [atom.lit , Ex.singleton atom] !env) dep;
                                     ignore (T.expensive_processing !env);
                                     assert false
                                     with Ex.Inconsistent dep ->
                                     ;()*)

let theory_analyze dep = 
  let atoms, sz, max_lvl = 
    Ex.fold
      (fun a (acc, sz,max_lvl) -> 
	if a.var.level = 0 then acc, sz, max_lvl
	else a.opp :: acc, sz + 1, max max_lvl a.var.level
      )dep ([], 0,0)
  in
  if atoms = [] then begin
    check_inconsistence_of dep;
    raise Unsat; (* une conjonction de faits unitaires etaient deja unsat *)
  end;
  let c_clause = mk_clause atoms sz 2 in
  c_clause.removed <- true;

  if D.d then eprintf "conflicting clause: %a@." pr_clause c_clause;
  let pathC  = ref 0 in
  let learnt = ref [] in
  let cond   = ref true in
  let blevel = ref 0 in
  let seen   = ref [] in
  let c      = ref c_clause in
  let tr_ind = ref (Vec.size env.trail - 1) in
  let size   = ref 1 in
  while !cond do
    if !c.learnt then clause_bump_activity !c;

    (* visit the current predecessors *)
    for j = 0 to Vec.size !c.atoms - 1 do
      let q = Vec.get !c.atoms j in
      (*printf "I visit %a@." D.atom q;*)
      assert (q.is_true || q.opp.is_true && q.var.level >= 0); (* Pas sur *)
      if not q.var.seen && q.var.level > 0 then begin
        var_bump_activity q.var;
        q.var.seen <- true;
        seen := q :: !seen;
        if q.var.level >= max_lvl then incr pathC
        else begin
          learnt := q :: !learnt; 
          incr size;
          blevel := max !blevel q.var.level
        end
      end
    done;
    
    (* look for the next node to expand *)
    while not (Vec.get env.trail !tr_ind).var.seen do decr tr_ind done;
    decr pathC;
    let p = Vec.get env.trail !tr_ind in
    decr tr_ind;
    match !pathC, p.var.reason with
      | 0, _ -> 
        cond := false;
        learnt := p.opp :: (List.rev !learnt)
      | n, None   -> assert false
      | n, Some cl -> c := cl
  done;
  List.iter (fun q -> q.var.seen <- false) !seen;
  !blevel, !learnt, !size

(*
  let rec all_propagations () = 
  match propagate() with
  | Some fclause -> 
(* XXX: atoms are negated *)
  Some (Ex.from_empty_clause fclause), 1
  | None -> 
  match theory_propagate () with
  | (Some _) as confl -> confl, 2
  | None ->
  let tenv, opt = Theory.learn env.tenv in
  env.tenv <- tenv;
  match opt with
  | Some (hyp,conc) -> 
  if false then 
  fprintf fmt
  "theory_propagation de %a -> %a@."
  Ex.print hyp pr_atom conc;
  let size = ref 1 in
  let learnt = 
  Ex.fold (fun a l ->  incr size; a.opp::l) hyp [] in
  record_theory_learnt_clause (conc::learnt) !size;
  all_propagations ()
  | None -> None, 0
*)

let rec all_propagations () = 
  match propagate() with
    | Some fclause -> 
        (* XXX: atoms are negated *)
      Some (Ex.from_empty_clause fclause), 1
    | None -> 
      match theory_propagate () with
        | (Some _) as confl -> confl, 2
        | None ->
          let tenv, l = Theory.learn env.tenv in
          env.tenv <- tenv;
          match l with
            | [] -> None, 0
            | l  -> 
              List.iter
                (fun (hyp,conc) -> 
                  if not conc.is_true then begin
                    if !dsat then 
                      fprintf fmt
                        "theory_propagation de %a -> %a@."
                        Ex.print hyp pr_atom conc;
                    let size = ref 1 in
                    let learnt = 
                      Ex.fold (fun a l ->  incr size; a.opp::l) hyp [] in
                    record_theory_learnt_clause (conc::learnt) !size;
                  end;
                )l;
              all_propagations ()

                
let search n_of_conflicts n_of_learnts = 
  let conflictC = ref 0 in
  env.starts <- env.starts + 1;
  while (true) do
    try match all_propagations () with
      | Some dep, source -> (* Conflict *)
	incr conflictC;
	env.conflicts <- env.conflicts + 1;
	D.conflict source dep (decision_level()) conflictC; 
        if D.d_decisions then
          eprintf "explication: %a@." Ex.print dep;
	if decision_level() = 0 then raise Unsat; (* T-L conflict *)
	let blevel, learnt, size = theory_analyze dep in
	cancel_until blevel;
	record_learnt_clause blevel learnt size
	  
      | None, _ -> 
	if nb_assigns() = env.nb_init_vars then raise Sat;
	if n_of_conflicts >= 0 && !conflictC >= n_of_conflicts then 
	  begin
	    env.progress_estimate <- progress_estimate();
            cancel_until 0;
	    raise Restart
	  end;
	if decision_level() = 0 then simplify ();
	
	if n_of_learnts >= 0 && 
	  Vec.size env.learnts - nb_assigns() >= n_of_learnts then 
	  reduce_db n_of_learnts;
	
	env.decisions <- env.decisions + 1;
	
	new_decision_level();
	let next = pick_branch_lit () in
	let current_level = decision_level () in
	D.decide env.decisions current_level next.pa;
	assert (next.level < 0);
	enqueue next.pa current_level None
    with Sat ->
      match expensive_theory_propagate () with
        | Some dep -> 
          Options.enable_theory := true;
          incr conflictC;
	  env.conflicts <- env.conflicts + 1;
	  D.theory_conflict dep (decision_level()) conflictC; 
          if D.d_decisions then
            eprintf "explication: %a@." Ex.print dep;
	  if decision_level() = 0 then raise Unsat; (* T-L conflict *)
	  let blevel, learnt, size = theory_analyze dep in
	  cancel_until blevel;
	  record_learnt_clause blevel learnt size;
          
        | None -> raise Sat
  done
    
    
let solve () = 
  if env.unsat then raise Unsat;
  let n_of_conflicts = ref (to_float env.restart_first) in
  let n_of_learnts = ref ((to_float (nb_clauses())) *. env.learntsize_factor) in
  if not D.d_decisions then begin
    eprintf "============================[ Search Statistics ]==========================================@.";
    eprintf "| Conflicts |          ORIGINAL         |          LEARNT          | Progress | CPU (s)   |@.";
    eprintf "|           |    Vars  Clauses Literals |    Limit  Clauses Lit/Cl |          |           |@.";
    eprintf "===========================================================================================@.";
  end;
  try 
    if nb_assigns() = env.nb_init_vars then raise Sat;
    while true do
      if not D.d_decisions then begin
        eprintf "| %9d | %7d %8d %8d | %8d %8d %6.0f | %6.3f  %% | %8g |@."
          env.conflicts
          (Heap.size env.order)
          (nb_clauses()) 
          env.clauses_literals
          (to_int !n_of_learnts)
          (nb_learnts())
	  ((to_float env.learnts_literals) /. (to_float (nb_learnts())))
          (env.progress_estimate *. 100.)
          (Timer.General.get());
      end;
      begin try search (to_int !n_of_conflicts) (to_int !n_of_learnts);
        with Restart -> 
          Options.enable_theory := not !Options.enable_theory
      end;
      n_of_conflicts := !n_of_conflicts *. env.restart_inc;
      n_of_learnts   := !n_of_learnts *. env.learntsize_inc;
    done;
  with 
    | Sat -> 
      D.check_model env.vars env.clauses env.learnts;
      raise Sat
    | Unsat -> 
      env.unsat <- true; 
      raise Unsat

exception Trivial

let add_clause atoms = 
  if env.unsat then raise Unsat;
  assert (decision_level () = 0);
  try
    let atoms, size = 
      List.fold_left
	(fun (acc,size) a -> 
          insert_var_order a.var;
	  if a.is_true then raise Trivial;
	  if not a.opp.is_true then a::acc, size + 1
          else acc, size
	)([],0) atoms 
    in
    match atoms with
      | []    -> env.unsat <- true; raise Unsat
        
      | _::_::_ -> 
        let atoms = List.fast_sort (fun a b -> a.var.vid - b.var.vid) atoms in
        let clause = mk_clause atoms size 0 in           
        
        (*attach_clause clause;
          Vec.push env.clauses clause;
          D.add_clause clause;
        (*D.watched_of_a (Vec.get clause.atoms 0).neg;
          D.watched_of_a (Vec.get clause.atoms 1).neg;*)
        *)
        
        begin try
                env.dnet <- Dnet.update_dnet atoms env.dnet clause;
                attach_clause clause;
                Vec.push env.clauses clause;
                D.add_clause clause;
        (*D.watched_of_a (Vec.get clause.atoms 0).neg;
          D.watched_of_a (Vec.get clause.atoms 1).neg;*)
          with Dnet.Exists _ -> D.add_clause_exists clause
        end
          
      | [a]   ->  
        D.add_unit_clause a;
        enqueue a 0 None; 
        if propagate () <> None then begin
          env.unsat <- true; 
          raise Unsat
        end
  with Trivial -> ()
    
let add_clauses cnf = 
  List.iter add_clause cnf;
  match theory_propagate () with
      None -> ()
    | Some dep ->
      if D.d_decisions then 
        fprintf fmt "unsat-core: %a@." Ex.print dep;
      env.unsat <- true; 
      raise Unsat
        

let reset_refs () =
  for i = 0 to Vec.size env.vars - 1 do
    unassign_atom (Vec.get env.vars i).pa
  done;
  env.unsat <- false;   
  env.clauses <- Vec.make 0 dummy_clause; (*sera mis a jour lors du parsing*)
  env.learnts <- Vec.make 0 dummy_clause; (*sera mis a jour lors du parsing*)
  env.clause_inc <- 1.;
  env.var_inc <- 1.;
  env.vars <- Vec.make 0 dummy_var; (*sera mis a jour lors du parsing*)
  Vec.clear env.trail;
  Vec.clear env.trail_lim;
  env.qhead <- 0;
  env.simpDB_assigns <- -1;
  env.simpDB_props <- 0;
  env.order <- Heap.init 0; (* sera mis a jour dans solve *)
  env.progress_estimate <- 0.;
  env.restart_first <- 100; (*100;*) 
  env.learntsize_factor <- 1. /. 3. ;
  env.starts <- 0;
  env.propagations <- 0;
  env.conflicts <- 0;
  env.clauses_literals <- 0;
  env.learnts_literals <- 0;
  env.max_literals <- 0;
  env.tot_literals <- 0;
  env.nb_init_vars <- 0;
  env.nb_init_clauses <- 0;
  Vec.clear env.model;
  env.tenv <- T.dummy_empty;
  Vec.clear env.tenv_queue;
  Queue.clear env.tatoms_queue;
  env.dnet <- Dnet.Node Dnet.M.empty;
