{ 
  open Lexing
  open Parser
}

rule token = parse
  | '\t'             { token lexbuf }
  | ' '              { token lexbuf }
  | ';'  (_ # '\n')* { token lexbuf }
  | ['\n']           { token lexbuf }
  | "_"              { UNDERSCORE }
  | "("              { LPAREN }
  | ")"              { RPAREN }
  | "as"             { AS }
  | "forall"         { FORALL }
  | "exists"         { EXISTS }
  | "!"              { EXCLIMATIONPT }
  | "set-logic"      { SETLOGIC }
  | "set-option"     { SETOPTION }
  | "set-info"       { SETINFO }
  | "declare-sort"   { DECLARESORT }
  | "define-sort"    { DEFINESORT }
  | "declare-fun"    { DECLAREFUN }
  | "define-fun"     { DEFINEFUN }
  | "push"           { PUSH }
  | "pop"            { POP }
  | "assert"         { ASSERT }
  | "check-sat"      { CHECKSAT }
  | "get-assertions" { GETASSERT }
  | "get-proof"      { GETPROOF }
  | "get-unsat-core" { GETUNSATCORE }
  | "get-value"      { GETVALUE }
  | "get-assignment" { GETASSIGN }
  | "get-option"     { GETOPTION }
  | "get-info"       { GETINFO }
  | "QF_LIA"         { QF_LIA }
  | "exit"           { EXIT }
  | ":status"        { STATUS }
  | "sat"            { SAT }
  | "unsat"          { UNSAT }
  | "unknown"        { UNKNOWN }
  | "Int"            { INT }
  | "Bool"           { BOOL }
  | "+"              { PLUS }
  | "-"              { MINUS }
  | "*"              { TIMES }
  | ">="             { GE }
  | ">"              { GT }
  | "<="             { LE }
  | "<"              { LT }
  | "not"            { NOT }
  | "="              { EQ }
  | "ite"            { ITE }
  | "let"            { LET }
  | "and"            { AND }
  | "or"             { OR }
  | "true"           { TRUE }
  | "false"          { FALSE }

  |  '#' 'x' ['0'-'9' 'A'-'F' 'a'-'f']+            {HEXADECIMAL (lexeme lexbuf)}
  |  '#' 'b' ['0'-'1']+                            {BINARY (lexeme lexbuf)}
  |  ( '0' | ['1'-'9'] ['0'-'9']* )                {NUMERAL (lexeme lexbuf)}
  |  ( '0' | ['1'-'9'] ['0'-'9']* ) '.' ['0'-'9']+ {DECIMAL (lexeme lexbuf)}

  |  '|' ([ '!'-'~' ' ' '\n' '\t' '\r'] # ['\\' '|'])* '|'
      { SYMBOL (lexeme lexbuf) }
  |  ':' ['a'-'z' 'A'-'Z' '0'-'9' '+' '-' '/' '*' '=' '%' '?' '!' '.' '$' '_' '~' '&' '^' '<' '>' '@']+
      { KEYWORD (lexeme lexbuf) }
  |  ['a'-'z' 'A'-'Z' '+' '-' '/' '*' '=''%' '?' '!' '.' '$' '_' '~' '&' '^' '<' '>' '@'] ['a'-'z' 'A'-'Z' '0'-'9' '+' '-' '/' '*' '=''%' '?' '!' '.' '$' '_' '~' '&' '^' '<' '>' '@']*
      { SYMBOL (lexeme lexbuf) }
  |  '"' (([ '!'-'~' ' ' '\n' '\t' '\r' ] # ['\\' '"']) | ('\\' ['!'-'~' ' ' '\n' '\t' '\r'] ))* '"' { STRINGLIT (lexeme lexbuf) }
  | eof  { EOF }
  | _ {failwith ((lexeme lexbuf) ^ ": lexing error on line ???")}



