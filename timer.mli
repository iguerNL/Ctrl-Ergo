(******************************************************************************)
(** Ce module contient des modules permettant de profiler des fonctions
    bien cibl�es **)
(******************************************************************************)
module General : sig
  val start : unit -> unit
  val get   : unit -> float
end

module type S = sig
  val start : unit -> unit
  val stop  : unit -> unit
  val get   : unit -> float
end

module Simplex_main : S
module CC_assume : S
module CC_expensive_processing : S
module Solver_minimize_dep : S
module Fm_simplex_apply_mapping_to_rs : S
module Fm_simplex_apply_mapping_to_bounds_2 : S
module Fm_simplex_split_constrs : S
module Fm_simplex_clustering : S
module Tp_apply_mapping : S
