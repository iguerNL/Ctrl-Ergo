open Ast_translator
open Format
open Options

module S = Hstring.M.Set

let clustering unit non_unit = 
  Timer.Fm_simplex_clustering.start();
  let unit_non_unit, all_lvs =
    List.fold_left
      (fun (acc, all_lvs) cl ->
         let lvs = 
           List.fold_left (fun acc e -> 
             Combine.leaves_of_atom S.add e acc) S.empty cl 
         in
         ([cl], lvs) :: acc, S.union lvs all_lvs
      )([], S.empty) unit
  in
  let unit_non_unit, all_lvs =
    List.fold_left
      (fun (acc, all_lvs) cl ->
         let lvs =
           List.fold_left 
             (fun acc e -> 
               Combine.leaves_of_atom S.add e acc) S.empty cl 
         in
         ([cl], lvs) :: acc, S.union lvs all_lvs
      )(unit_non_unit, all_lvs) non_unit
  in
  fprintf fmt "leaves of atoms done: nb_leaves = %d@." (S.cardinal all_lvs);
  let blocs =
    S.fold
      (fun x cll_st ->
         let lx, l_nx = List.partition (fun (_,s) -> S.mem x s) cll_st in
         match lx with
           | [] -> assert false
           | e:: lx ->
               let elx = 
                 List.fold_left
                   (fun (l, lvs) (l', lvs') -> 
                      List.rev_append l l', S.union lvs lvs')
                   e lx
               in 
               elx :: l_nx
      )all_lvs unit_non_unit
  in 
  fprintf fmt "# nb de blocs: %d@." (List.length blocs);
  List.rev_map
    (fun (pb,_) ->
         List.fold_left
           (fun (u,nu) c -> 
              match c with [] -> assert false |[_] -> c::u, nu | _  -> u, c::nu)
           ([],[]) pb
    )blocs


let main unit non_unit = 
  if !Options.clustering then 
    let res = clustering unit non_unit in
    Dsteps.step_clustering ();
    if !clustering_only then raise Exit;
    res
  else 
    [unit, non_unit]
