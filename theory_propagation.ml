open Format
open Options
open Ast_translator
open Numbers
open Stdlib

module A = Arith
module C = Combine
module M = C.MDec
module Ex = Explanation

module MZ = Map.Make(Z)
module MEx= Map.Make(Ex)

module MV = Map.Make
  (struct
    type t = Stypes.var
    let compare a b = Stypes.cmp_var a b
   end)
  
module SV = MV.Set

type e = SV.t MZ.t MEx.t
type t = 
    {
      mp     : (e * e) M.t ;
      learnt : (Ex.t * Stypes.atom) list
    }

let empty = {mp=M.empty; learnt=[]}

let print env = 
  if !dtp then begin
    fprintf fmt "==TP=======================================================@.";
    M.iter
      (fun t (mex_eq,mex_geq) ->
        fprintf fmt "%a ->@." C.print t;
        MEx.iter
          (fun ex mz ->
            fprintf fmt "   %a: @." Ex.print ex;
            MZ.iter
              (fun c v -> fprintf fmt "       = %s@." (Z.to_string c)) mz
          )mex_eq;
        MEx.iter
          (fun ex mz ->
            fprintf fmt "   %a: @." Ex.print ex;
            MZ.iter
              (fun c v -> fprintf fmt "      >= %s@." (Z.to_string c)) mz
          )mex_geq
      )env.mp;
    fprintf fmt "==/TP======================================================@.";
  end

let init vars =
  if not !Options.theory_propagation then empty
  else
    let mp = ref M.empty in
    for i = 0 to Vec.size vars - 1 do
      let v = Vec.get vars i in
    (*fprintf fmt "%a@." Stypes.pr_var v;*)
      match v.Stypes.view with
        | Stypes.EQ (t,z) ->
          let ex0 = Ex.empty in
          let mex_eq, mex_geq = M.find_default t (MEx.empty,MEx.empty) !mp in
          let mz  = MEx.find_default ex0 MZ.empty mex_eq in
          let mz  = MZ.add z (SV.singleton v) mz in
          let mex_eq = MEx.add ex0 mz mex_eq in
          mp := M.add t (mex_eq, mex_geq) !mp
        | Stypes.GE (t,z) ->
          let ex0 = Ex.empty in
          let mex_eq, mex_geq = M.find_default t (MEx.empty,MEx.empty) !mp in
          let mz  = MEx.find_default ex0 MZ.empty mex_geq in
          let mz  = MZ.add z (SV.singleton v) mz in
          let mex_geq = MEx.add ex0 mz mex_geq in
          mp := M.add t (mex_eq, mex_geq) !mp

        | Stypes.TRUE | Stypes.VAR _ | Stypes.PROXY _ -> ()
    done;
    let env = { mp = !mp; learnt = [] } in
    print env;
    env


let update_m x mex_eq mex_geq mp = 
  try
    let mex_eq0, mex_geq0 = M.find x mp in
    let mex_eq = 
      MEx.union 
        (fun ex mz1 mz2 -> 
          Some (MZ.union (fun c sv1 sv2 -> Some (SV.union sv1 sv2)) mz1 mz2)
        ) mex_eq mex_eq0
    in
    let mex_geq = 
      MEx.union 
        (fun ex mz1 mz2 -> 
          Some (MZ.union (fun c sv1 sv2 -> Some (SV.union sv1 sv2)) mz1 mz2)
        ) mex_geq mex_geq0 
    in
    M.add x (mex_eq, mex_geq) mp
  with Not_found -> M.add x (mex_eq, mex_geq) mp
    

let eq_deductions mex_eq learnt d exp =
  MEx.fold
    (fun ex mz learnt ->
      let ex = Ex.union ex exp in
      MZ.fold
        (fun c sv learnt -> 
          if not (Z.equal c d) then
            SV.fold
              (fun v learnt ->
                if not v.Stypes.pa.Stypes.is_true &&
                  not v.Stypes.na.Stypes.is_true then begin
                    if !dtp then 
                      fprintf fmt "Je deduis: %a -> %a@." 
                        Ex.print ex Stypes.pr_atom v.Stypes.na;
                    (ex, v.Stypes.na) :: learnt
                  end
                else learnt
              )sv learnt
          else begin
            SV.fold
              (fun v learnt ->
                if not v.Stypes.pa.Stypes.is_true &&
                  not v.Stypes.na.Stypes.is_true then begin
                    if !dtp then 
                      fprintf fmt "Je deduis: %a -> %a@." 
                        Ex.print ex Stypes.pr_atom v.Stypes.pa;
                    (ex, v.Stypes.pa) :: learnt
                  end
                else learnt
              )sv learnt
          end
        )mz learnt
    )mex_eq learnt


let geq_deductions mex_geq learnt d exp =
  MEx.fold
    (fun ex mz learnt ->
      let ex = Ex.union ex exp in
      MZ.fold
        (fun c sv learnt -> 
          if Z.compare d c < 0 then
            SV.fold
              (fun v learnt ->
                if not v.Stypes.pa.Stypes.is_true &&
                  not v.Stypes.na.Stypes.is_true then begin
                    if !dtp then 
                      fprintf fmt "Je deduis: %a -> %a@." 
                        Ex.print ex Stypes.pr_atom v.Stypes.na;
                    (ex, v.Stypes.na) :: learnt
                  end
                else learnt
              )sv learnt
          else begin
            SV.fold
              (fun v learnt ->
                if not v.Stypes.pa.Stypes.is_true &&
                  not v.Stypes.na.Stypes.is_true then begin
                    if !dtp then 
                      fprintf fmt "Je deduis: %a -> %a@." 
                        Ex.print ex Stypes.pr_atom v.Stypes.pa;
                    (ex, v.Stypes.pa) :: learnt
                  end
                else learnt
              )sv learnt
          end
        )mz learnt
    )mex_geq learnt

let subst_in_val p v exp x (mex_eq,mex_geq) {mp=mp; learnt=ma} =
  let y = C.subst p v x in
  if false then fprintf fmt "on %a gives %a@." C.print x C.print y;
  if C.equal x y then begin
    if false then fprintf fmt "1@.";
    {mp=update_m x mex_eq mex_geq mp; learnt=ma}
  end
  else begin
    match y with
      | Arith {m=m; c=d} when M.is_empty m -> 
        if false then fprintf fmt "2@.";
        let ma = eq_deductions mex_eq ma d exp in
        let ma = geq_deductions mex_geq ma d exp in
        {mp=mp; learnt=ma }
      | _ -> 
        if false then fprintf fmt "3@.";
        let mex_eq = 
          MEx.fold
            (fun ex mz mex -> MEx.add (Ex.union exp ex) mz mex)
            mex_eq MEx.empty
        in
        let mex_geq = 
          MEx.fold
            (fun ex mz mex -> MEx.add (Ex.union exp ex) mz mex)
            mex_geq MEx.empty
        in
        {mp=update_m y mex_eq mex_geq mp; learnt=ma}
  end

let apply_mapping env p v exp = 
  if not !Options.theory_propagation then env
  else begin
    Timer.Tp_apply_mapping.start();
    if !dtp then fprintf fmt "apply_mapping: %a |-> %a@." C.print p C.print v;
    print env;
    let env = 
      if true then 
        try subst_in_val p v exp p (M.find p env.mp) env with Not_found -> env
      else 
        M.fold (subst_in_val p v exp) env.mp {env with mp = M.empty} 
    in
    print env;
    Timer.Tp_apply_mapping.stop();
    env
  end


let apply_ineq_neg env t c exp = 
  try 
    let eq, geq = M.find t env.mp in
    let l = ref env.learnt in
    MEx.iter
      (fun ex mz ->
        let ex = Ex.union ex exp in
        MZ.iter 
          (fun z sv ->
            if !dtp then
              fprintf fmt "z = %s et c = %s@." (Z.to_string z) (Z.to_string c);
            if Z.compare c z < 0 then 
              SV.iter
                (fun v ->
                  if not v.Stypes.pa.Stypes.is_true &&
                    not v.Stypes.na.Stypes.is_true then begin
                      if !dtp then 
                        fprintf fmt "Je deduis 4: %a -> %a@.@." 
                          Ex.print ex Stypes.pr_atom v.Stypes.na;
                      l := (ex, v.Stypes.na) :: !l
                    end
                )sv
          )mz; 
      )eq;
    MEx.iter
      (fun ex mz ->
        let ex = Ex.union ex exp in
        MZ.iter 
          (fun z sv ->
            if !dtp then
              fprintf fmt "z = %s et c = %s@." (Z.to_string z) (Z.to_string c);
            if Z.compare c z < 0 then 
              SV.iter
                (fun v ->
                  if not v.Stypes.pa.Stypes.is_true &&
                    not v.Stypes.na.Stypes.is_true then begin
                      if !dtp then 
                        fprintf fmt "Je deduis 5: %a -> %a@.@." 
                          Ex.print ex Stypes.pr_atom v.Stypes.na;
                      l := (ex, v.Stypes.na) :: !l
                    end
                )sv
          )mz; 
      )geq;
    { env with learnt = !l }
  with Not_found -> env


let apply_ineq_pos env t c exp =
  try
    let eq, geq = M.find t env.mp in
    let l = ref env.learnt in
    MEx.iter
      (fun ex mz ->
        let ex = Ex.union ex exp in
        MZ.iter 
          (fun z sv ->
            if !dtp then
              fprintf fmt "z = %s et c = %s@." (Z.to_string z) (Z.to_string c);
            if Z.compare z c < 0 then 
              SV.iter
                (fun v ->
                  if not v.Stypes.pa.Stypes.is_true &&
                    not v.Stypes.na.Stypes.is_true then begin
                      if !dtp then 
                        fprintf fmt "Je deduis 2: %a -> %a@.@." 
                          Ex.print ex Stypes.pr_atom v.Stypes.na;
                      l := (ex, v.Stypes.na) :: !l
                    end
                )sv
          )mz; 
      )eq;
    MEx.iter
      (fun ex mz ->
        let ex = Ex.union ex exp in
        MZ.iter 
          (fun z sv ->
            if !dtp then
              fprintf fmt "z = %s et c = %s@." (Z.to_string z) (Z.to_string c);
            if Z.compare z c < 0 then 
              SV.iter
                (fun v ->
                  if not v.Stypes.pa.Stypes.is_true &&
                    not v.Stypes.na.Stypes.is_true then begin
                      if !dtp then 
                        fprintf fmt "Je deduis 3: %a -> %a@.@." 
                          Ex.print ex Stypes.pr_atom v.Stypes.pa;
                      l := (ex, v.Stypes.pa) :: !l
                    end
                )sv
          )mz; 
      )geq;
    { env with learnt = !l }
  with Not_found -> env
    
let apply_ineq env t c exp is_neg = 
  if not !Options.theory_propagation then env
  else begin
    Timer.Tp_apply_mapping.start();
    if !dtp then 
      fprintf fmt "apply_ineq: %b (%a >= %s)@." 
        is_neg C.print t (Z.to_string c);
    let res = 
      if is_neg then apply_ineq_neg env t c exp
      else apply_ineq_pos env t c exp
    in
    Timer.Tp_apply_mapping.stop();
    res
  end


let apply_bounds env new_px int = env(*
  let env = match Intervals.borne_inf int with
      | None -> env
      | Some (n,exp) -> 
        assert (Z.is_one (Q.denominator n));
        apply_ineq env new_px (Q.numerator n) exp false
  in
  let env = match Intervals.borne_sup int with
      | None -> env
      | Some (n,exp) -> 
        assert (Z.is_one (Q.denominator n));
        let n = Z.add (Q.numerator n) Z.one  in
        apply_ineq env new_px n exp true
  in
  env
                                     *)

let learn env =
  if not !Options.theory_propagation then env, []
  else
    (*let l = 
      List.filter
        (fun (_,a) -> not a.Stypes.is_true && not a.Stypes.opp.Stypes.is_true) 
        env.learnt
    in*)
    {env with learnt = []}, env.learnt(*l*)
