open Format
open Options
open Numbers
open Ast_translator

module Ex = Explanation
module A = Arith
module C = Combine
module Tp = Theory_propagation
module MD = C.MDec
module SD = MD.Set

type t = 
    { rs         : (C.t * Ex.t) MD.t;
      pending_rs : C.t list;
      ub_rs      : SD.t MD.t }
      
let empty = 
  { rs = MD.empty;
    pending_rs = [];
    ub_rs = MD.empty }
    
let merge ieqs1 ieqs2 = 
  { rs = MD.union (fun x -> 
    fprintf fmt "%a@." C.print x;
    assert false) ieqs1.rs ieqs2.rs;
    ub_rs = MD.union (fun _ -> assert false) ieqs1.ub_rs ieqs2.ub_rs;
    pending_rs = List.rev_append ieqs1.pending_rs ieqs2.pending_rs }
    

let env_nform {rs=rs} (r, ex_r) =
  List.fold_left
    (fun (newr,ex) x -> 
      try
        let rc_x, ex_x = MD.find x rs in 
        C.subst x rc_x newr, Ex.union ex ex_x
      with Not_found -> newr, ex) (r, ex_r) (C.xleaves r)
    
let pending_rs ({pending_rs=p_rs; rs=rs} as env) = 
  let p_rs = 
    List.rev_map
      (fun x -> try (x, MD.find x rs) with Not_found -> assert false) p_rs in
  p_rs, {env with pending_rs = []}

let print {rs=rs} = 
  if !dla then begin
    fprintf fmt "---------- Rewriting System -----------------------@.";
    MD.iter 
      (fun x (rx,ex) -> 
        fprintf fmt "%a -> %a : %a@." C.print x C.print rx Ex.print ex
      ) rs;
    (*fprintf fmt "---------- Used_by RS -----------------------------@.";
      MD.iter 
      (fun x st -> 
      fprintf fmt "%a -> %a@." C.print x pset st
      )env.ub_rs;*)
    fprintf fmt "---------------------------------------------------@.@."
  end

let add_to_ub_rs p v ub_rs = 
  List.fold_left
    (fun acc lf ->
      let rep_uses_lf = try MD.find lf acc with Not_found -> SD.empty in
      MD.add lf (SD.add p rep_uses_lf) acc
    )ub_rs (C.xleaves v)
    
let remove_from_ub_rs p v ub_rs =
  List.fold_left
    (fun acc lf ->
      let rep_uses_lf = try MD.find lf acc with Not_found -> SD.empty in
      MD.add lf (SD.remove p rep_uses_lf) acc
    )ub_rs (C.xleaves v)


let apply_mapping_to_rs ex (env, tp) (p,v) =
  let ub_rs = add_to_ub_rs p v env.ub_rs in
  let uses_p = try MD.find p ub_rs with Not_found -> SD.empty in
  let rs, ub_rs = 
    SD.fold
      (fun x  (rs, ub_rs) ->
        let rc_x, ex_x = try MD.find x rs with Not_found -> assert false in
        let new_rc_x = C.subst p v rc_x in
        if C.equal new_rc_x rc_x then rs, ub_rs
        else begin
          let ex_u = Ex.union ex_x ex in
          let rs = MD.add x (new_rc_x, ex_u) rs in
          let ub_rs = remove_from_ub_rs x rc_x ub_rs in
          let ub_rs = add_to_ub_rs x new_rc_x ub_rs in
          rs, ub_rs
        end
      )uses_p (env.rs, ub_rs)
  in
  let ub_rs = MD.remove p ub_rs in
  {env with rs = MD.add p (v,ex) rs; ub_rs=ub_rs },
  Tp.apply_mapping tp p v ex

let solve_eq env tp (px, n, ex) =
  let nx = Arith {m=MD.empty; c=n} in
  let tp = Tp.apply_mapping tp px nx ex  in
  (*Debug.solve_eq px n ex;*)
  let rpx, ex = env_nform env (px, ex) in
  let rp = C.arith_of_alien rpx in
  let nxx = Arith {m=MD.empty; c=Z.sub n rp.c} in
  let rpxx = Arith {rp with c=Z.zero} in
  let tp = Tp.apply_mapping tp rpxx nxx ex in
  try 
    let sbt = C.solve rpx n in
    Timer.Fm_simplex_apply_mapping_to_rs.start();
    let env, tp = List.fold_left (apply_mapping_to_rs ex) (env,tp) sbt in
    Timer.Fm_simplex_apply_mapping_to_rs.stop();
    let env = 
      List.fold_left 
        (fun env (p,v) -> 
          {env with pending_rs = p :: env.pending_rs}) env sbt in
    env, tp
  with Unsolvable -> raise (Ex.Inconsistent ex)
