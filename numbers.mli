(******************************************************************************)
(** Ce fichier founit deux modules pour manipuler des entiers et des rationnels
    avec une precision arbitraire **)
(******************************************************************************)

module Z : sig
  type t
  
  val zero : t 
  val one : t
  val minus_one : t
  val is_zero : t -> bool
  val is_one : t -> bool
  val is_minus_one : t -> bool
  val compare : t -> t -> int
  val compare_to_0 : t -> int
  val equal   : t -> t -> bool

  val add : t -> t -> t
  val sub : t -> t -> t
  val mult : t -> t -> t
  val div : t -> t -> t
  val div_rem : t -> t -> t * t
  val minus : t -> t
  val abs  : t -> t
  
  val of_int : int -> t
  val of_string : string -> t
  val to_string : t -> string
  val pgcd : t -> t -> t
  val ppmc : t -> t -> t
  val hash : t -> int
  val sign : t -> int
end

module Q : sig

  type t

  val zero : t 
  val one : t
  val minus_one : t
  val inf : t

  val is_zero : t -> bool
  val is_one : t -> bool
  val is_minus_one : t -> bool

  val of_int : int -> t
  val of_z : Z.t -> t
  val of_zz: Z.t -> Z.t -> t
  val of_string : string -> t
  val to_string : t -> string

  val compare : t -> t -> int
  val compare_to_0 : t -> int
  val equal   : t -> t -> bool
  
  val add : t -> t -> t
  val sub : t -> t -> t
  val mult : t -> t -> t
  val div : t -> t -> t
  val minus : t -> t
  val inv : t -> t
  val modulo : t -> t -> t
  val floor : t -> t
  val ceiling : t -> t
  val abs : t -> t
  val power : t -> t -> t
  val numerator : t -> Z.t
  val denominator : t -> Z.t
  val to_float : t -> float
  val is_integer : t -> bool

  val root_default : t -> int -> t
  val root_exces : t -> int -> t
  val sign : t -> int

  val min : t -> t -> t
end
