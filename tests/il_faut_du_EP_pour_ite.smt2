(set-logic QF_LIA)
(set-info :status unknown)
(declare-fun P () Bool)
(declare-fun a () Int)
(declare-fun b () Int)
(declare-fun c () Int)

(assert 
 (let 
     ((v (ite P 0 (ite P a b))))
   (= c v)
   )
 )

Naive formula
;;;;;;;;;;;;;;;;;;;;
Conj---> 
(and
  (or
    (and
      (and
        not (P),
        not (P)
      ),
      b-v=0
    ),
    (and
      P,
      v=0
    ),
    (and
      (and
        P,
        not (P)
      ),
      a-v=0
    )
  ),
  c-v=0
)
1 --> 3 --> not (P)
0 --> 2 --> P
8 --> 1 --> v=0
12 --> 1 --> b-v=0
16 --> 1 --> a-v=0
22 --> 1 --> c-v=0
Simplified formula
;;;;;;;;;;;;;;;;;;;;
Conj---> 
(and
  c-v=0,
  (or
    (and
      b-v=0,
      not (P)
    ),
    (and
      v=0,
      P
    )
  )
)